package com.revature.unittesting.cucumberselenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
    public WebDriver driver;
    @FindBy(id = "loginbutton")
    public WebElement loginButton;
    @FindBy(id = "user")
    public WebElement userInput;
    @FindBy(id = "userpassword")
    public WebElement passwordInput;
    @FindBy(id = "logoutbutton")
    public WebElement logoutButton;



    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }
}
