package com.revature.unittesting.cucumberselenium.steps;

import com.revature.unittesting.cucumberselenium.pages.LoginPage;
import com.revature.unittesting.cucumberselenium.runners.TRMSRunner;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

public class LoginPageStepImpl {
    public static LoginPage loginPage = TRMSRunner.loginPage;
    public static WebDriver webDriver = TRMSRunner.driver;
    @Given("^the user is on the login page$")
    public void the_user_is_on_the_login_page() throws Throwable{
        webDriver.get("file:///C:/Users/Michael/Desktop/html%20code/project1/login.html");
    }
    @When("the user logs in with credentials$")
    public void the_user_logs_in_with_credentials() throws Throwable{
        loginPage.userInput.sendKeys("kel.smith");
        loginPage.passwordInput.sendKeys("apassword");
        loginPage.loginButton.click();
        try{
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Then("^the user should be on the usersrequests page$")
    public void the_user_should_be_on_the_usersrequests_page() throws Throwable{
        Assert.assertEquals("Create Request",webDriver.getTitle());
    }

    @Given("^the user is on the create request page$")
    public void the_user_is_on_the_create_request_page() throws Throwable{
        webDriver.get("file:///C:/Users/Michael/Desktop/html%20code/project1/createrequest.html");
    }
    @When("^the user clicks log out$")
    public void the_user_clicks_log_out() throws Throwable{
        loginPage.logoutButton.click();
    }
    @Then("^the user is logged out$")
    public void the_user_is_logged_out() throws Throwable{
        Assert.assertEquals("Login", webDriver.getTitle());
    }
}
