package com.revature.unittesting.cucumberselenium.runners;

import com.revature.unittesting.cucumberselenium.pages.LoginPage;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources", glue = {"com.revature.unittesting.cucumberselenium.steps"})
public class TRMSRunner {
    public static WebDriver driver;
    public static LoginPage loginPage;

    @BeforeClass
    public static void setUp(){
        String path = "C:/Users/Michael/Desktop/chromedriver.exe";
        System.setProperty("webdriver.chrome.driver", path);
        driver = new ChromeDriver();
        loginPage = new LoginPage(driver);
        try{
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @AfterClass
    public static void tearDown(){
        driver.quit();
    }

}
