package com.revature.unittesting.DAO;

import com.revature.models.Employee;
import com.revature.repo.*;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EmployeeDAOTesting {
    EmployeeRepo er = new EmployeeRepoImpl();
    DepartmentsRepo dr = new DepartmentsRepoImpl();
    RolesRepo rr = new RolesRepoImpl();

    @Test
    public void getEmployee (){
        Employee expected = new Employee(1,"John","Doe","john.doe","password",dr.getDepartment(5),rr.getRoles(1),er.getEmployee(5));
        Employee actual = er.getEmployee(1);
        assertEquals(expected,actual);
    }
    @Test
    public void addEmployee(){
        Employee expected = new Employee(44,"testfname","testlname","testusername","testpassword",dr.getDepartment(1),rr.getRoles(1), null);
        Employee actual = new Employee(44,"testfname","testlname","testusername","testpassword",dr.getDepartment(1),rr.getRoles(1), null);
        actual = er.addEmployee(actual);
        expected.setId(actual.getId());
        assertEquals(actual,expected);
        er.deleteEmployee(actual.getId());

    }
    @Test
    public void getAllEmployee(){
        List<Employee> EmployeeList = er.getAllEmployees();
        assertTrue(EmployeeList.size()>0);
    }
    @Test
    public void updateEmployee () {
        Employee r = er.addEmployee(new Employee(44,"testfname","testlname","testusername","testpassword",dr.getDepartment(1),rr.getRoles(1), null));
        r.setFirstname("Tested");
        Employee updatedR = er.updateEmployee(r);
        assertEquals(r,updatedR);
        er.deleteEmployee(r.getId());
    }
    @Test
    public void deleteEmployee() {
        Employee r = er.addEmployee(new Employee(44,"testfname","testlname","testusername","testpassword",dr.getDepartment(1),rr.getRoles(1), null));
        Employee deletedR = er.deleteEmployee(r.getId());
        assertEquals(r, deletedR);
    }
}
