package com.revature.unittesting.DAO;

import com.revature.models.Request;
import com.revature.repo.*;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RequestDAOTesting {
    RequestRepo rr = new RequestRepoImpl();
    GradingFormatsRepo gfr = new GradingFormatsRepoImpl();
    TuitionTypesRepo ttr = new TuitionTypesRepoImpl();
    EmployeeRepo er = new EmployeeRepoImpl();
    RequestStatusesRepo rsr = new RequestStatusesRepoImpl();
    @Test
    public void getRequest (){
        Request expected = new Request(41,1637703300000L,200,0, false,"A", "to makes moneys","bitcoin","Pomona, California", gfr.getGradingFormat(1),ttr.getTuitionTypes(2),
                er.getEmployee(6), rsr.getRequestStatus(22),1635194164000L,null, null,0);
        Request actual = rr.getRequest(41);
        assertEquals(expected,actual);
    }
    @Test
    public void addRequest(){
        Request expected = new Request(4444,4444444444444L,444,4, false,"4", "44444","44444","44, 44", gfr.getGradingFormat(1),ttr.getTuitionTypes(2),
                er.getEmployee(6), rsr.getRequestStatus(22),444444444L,null, null,4);
        Request actual = new Request(4444,4444444444444L,444,4, false,"4", "44444","44444","44, 44", gfr.getGradingFormat(1),ttr.getTuitionTypes(2),
                er.getEmployee(6), rsr.getRequestStatus(22),444444444L,null, null,4);
        actual = rr.addRequest(actual);
        expected.setId(actual.getId());
        assertEquals(actual,expected);
        rr.deleteRequest(actual.getId());

    }
    @Test
    public void getAllRequest(){
        List<Request> RequestList = rr.getAllRequests();
        assertTrue(RequestList.size()>0);
    }
    @Test
    public void updateRequest () {
        Request r = rr.addRequest(new Request(4444,4444444444444L,444,4, false,"4", "44444","44444","44, 44", gfr.getGradingFormat(1),ttr.getTuitionTypes(2),
                er.getEmployee(6), rsr.getRequestStatus(22),444444444L,null, null,4));
        r.setDescription("Tested");
        Request updatedR = rr.updateRequest(r);
        assertEquals(r,updatedR);
        rr.deleteRequest(r.getId());
    }
    @Test
    public void deleteRequest() {
        Request r = rr.addRequest(new Request(4444,4444444444444L,444,4, false,"4", "44444","44444","44, 44", gfr.getGradingFormat(1),ttr.getTuitionTypes(2),
                er.getEmployee(6), rsr.getRequestStatus(22),444444444L,null, null,4));
        Request deletedR = rr.deleteRequest(r.getId());
        assertEquals(r, deletedR);
    }
}
