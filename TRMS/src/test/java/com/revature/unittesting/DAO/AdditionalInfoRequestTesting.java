package com.revature.unittesting.DAO;

import com.revature.models.AdditionalInfoRequest;
import com.revature.repo.AdditionalInfoRequestRepo;
import com.revature.repo.AdditionalInfoRequestRepoImpl;
import com.revature.repo.RequestRepo;
import com.revature.repo.RequestRepoImpl;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AdditionalInfoRequestTesting {
    AdditionalInfoRequestRepo ar = new AdditionalInfoRequestRepoImpl();
    RequestRepo rr = new RequestRepoImpl();
    @Test
    public void getAdditionalInfoRequest (){
        AdditionalInfoRequest expected = new AdditionalInfoRequest(1,"testing",rr.getRequest(41));
        AdditionalInfoRequest actual = ar.getAdditionalInfoRequest(1);
        actual.initRequest();
        assertEquals(expected,actual);
    }
    @Test
    public void addAdditionalInfoRequest(){
        AdditionalInfoRequest expected = new AdditionalInfoRequest(44,"Test",rr.getRequest(4));
        AdditionalInfoRequest actual = new AdditionalInfoRequest(44,"Test",rr.getRequest(4));
        actual = ar.addAdditionalInfoRequest(actual);
        expected.setId(actual.getId());
        assertEquals(actual,expected);
        ar.deleteAdditionalInfoRequest(actual.getId());

    }
    @Test
    public void getAllAdditionalInfoRequest(){
        List<AdditionalInfoRequest> AdditionalInfoRequestList = ar.getAllAdditionalInfoRequests();
        assertTrue(AdditionalInfoRequestList.size()>0);
    }
    @Test
    public void updateAdditionalInfoRequest () {
        AdditionalInfoRequest r = ar.addAdditionalInfoRequest(new AdditionalInfoRequest(44,"Test",rr.getRequest(4)));
        r.setFilePath("Tested");
        AdditionalInfoRequest updatedR = ar.updateAdditionalInfoRequest(r);
        assertEquals(r,updatedR);
        ar.deleteAdditionalInfoRequest(r.getId());
    }
    @Test
    public void deleteAdditionalInfoRequest() {
        AdditionalInfoRequest r = ar.addAdditionalInfoRequest(new AdditionalInfoRequest(44,"Test",rr.getRequest(4)));
        AdditionalInfoRequest deletedR = ar.deleteAdditionalInfoRequest(r.getId());
        assertEquals(r, deletedR);
    }
}
