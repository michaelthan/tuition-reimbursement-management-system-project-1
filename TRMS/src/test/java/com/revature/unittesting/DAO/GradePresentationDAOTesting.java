package com.revature.unittesting.DAO;

import com.revature.models.Employee;
import com.revature.models.GradePresentation;
import com.revature.models.Request;
import com.revature.repo.*;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class GradePresentationDAOTesting {
    GradePresentationRepo gr = new GradePresentationRepoImpl();
    RequestRepo rr = new RequestRepoImpl();
    EmployeeRepo er = new EmployeeRepoImpl();
    @Test
    public void getGradePresentation (){
        GradePresentation expected = new GradePresentation(36,"test", false, true,rr.getRequest(41),er.getEmployee(1));
        GradePresentation actual = gr.getGradePresentation(36);
        assertEquals(expected,actual);
    }
    @Test
    public void addGradePresentation(){
        GradePresentation expected = new GradePresentation(44,"Test", false, false, rr.getRequest(41),er.getEmployee(1));
        GradePresentation actual = new GradePresentation(44,"Test", false, false, rr.getRequest(41),er.getEmployee(1));
        actual = gr.addGradePresentation(actual);
        expected.setId(actual.getId());
        assertEquals(actual,expected);
        gr.deleteGradePresentation(actual.getId());

    }
    @Test
    public void getAllGradePresentation(){
        List<GradePresentation> GradePresentationList = gr.getAllGradePresentations();
        assertTrue(GradePresentationList.size()>0);
    }
    @Test
    public void updateGradePresentation () {
        GradePresentation r = gr.addGradePresentation(new GradePresentation(44,"Test", false, false, rr.getRequest(41),er.getEmployee(1)));
        r.setFilePath("Tested");
        GradePresentation updatedR = gr.updateGradePresentation(r);
        assertEquals(r,updatedR);
        gr.deleteGradePresentation(r.getId());
    }
    @Test
    public void deleteGradePresentation() {
        GradePresentation r = gr.addGradePresentation(new GradePresentation(44,"Test", false, false, rr.getRequest(41),er.getEmployee(1)));
        GradePresentation deletedR = gr.deleteGradePresentation(r.getId());
        assertEquals(r, deletedR);
    }
}
