package com.revature.unittesting.DAO;

import com.revature.models.Roles;
import com.revature.repo.RolesRepo;
import com.revature.repo.RolesRepoImpl;
import org.junit.jupiter.api.Test;

import javax.management.relation.Role;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class RolesDAOTesting {
    RolesRepo rr = new RolesRepoImpl();

    @Test
    public void getRole (){
        Roles expected = new Roles(1,"Employee");
        Roles actual = rr.getRoles(1);
        assertEquals(expected,actual);
    }
    @Test
    public void addRole(){
        Roles expected = new Roles(44,"Test");
        Roles actual = new Roles(44,"Test");
        actual = rr.addRole(actual);
        expected.setId(actual.getId());
        assertEquals(actual,expected);
        rr.deleteRole(actual.getId());

    }
    @Test
    public void getAllRoles(){
        List<Roles> rolesList = rr.getAllRoles();
        assertTrue(rolesList.size()>0);
    }
    @Test
    public void updateRole () {
        Roles r = rr.addRole(new Roles(44,"Test"));
        r.setRole("Tested");
        Roles updatedR = rr.updateRole(r);
        assertEquals(r,updatedR);
        rr.deleteRole(r.getId());
    }
    @Test
    public void deleteRole() {
        Roles r = rr.addRole(new Roles(44, "Test"));
        Roles deletedR = rr.deleteRole(r.getId());
        assertEquals(r, deletedR);
    }
}
