package com.revature.unittesting.DAO;

import com.revature.models.ReimbursementAmountAlteration;
import com.revature.repo.*;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ReimbursementAmountAlterationDAOTesting {
    ReimbursementAmountAlterationRepo rr = new ReimbursementAmountAlterationRepoImpl();
    RequestRepo r = new RequestRepoImpl();
    EmployeeRepo er = new EmployeeRepoImpl();
    @Test
    public void getReimbursementAmountAlteration (){

        ReimbursementAmountAlteration expected = new ReimbursementAmountAlteration(1,200,444,"because",r.getRequest(41),er.getEmployee(4));
        ReimbursementAmountAlteration actual = rr.getReimbursementAmountAlteration(1);
        expected.initRequest();
        expected.initAlteredBy();
        actual.initAlteredBy();
        actual.initRequest();
        assertEquals(expected,actual);
    }
    @Test
    public void addReimbursementAmountAlteration(){
        ReimbursementAmountAlteration expected = new ReimbursementAmountAlteration(44,44444,44444,"Test",r.getRequest(41),er.getEmployee(4));
        ReimbursementAmountAlteration actual = new ReimbursementAmountAlteration(44,44444,44444,"Test",r.getRequest(41),er.getEmployee(4));
        actual = rr.addReimbursementAmountAlteration(actual);
        expected.setId(actual.getId());
        assertEquals(actual,expected);
        rr.deleteReimbursementAmountAlteration(actual.getId());

    }
    @Test
    public void getAllReimbursementAmountAlteration(){
        List<ReimbursementAmountAlteration> ReimbursementAmountAlterationList = rr.getAllReimbursementAmountAlterations();
        assertTrue(ReimbursementAmountAlterationList.size()>0);
    }
    @Test
    public void updateReimbursementAmountAlteration () {
        ReimbursementAmountAlteration ra = rr.addReimbursementAmountAlteration(new ReimbursementAmountAlteration(44,44444,44444,"Test",r.getRequest(41),er.getEmployee(4)));
        ra.setReason("Tested");
        ReimbursementAmountAlteration updatedR = rr.updateReimbursementAmountAlteration(ra);
        assertEquals(ra,updatedR);
        rr.deleteReimbursementAmountAlteration(ra.getId());
    }
    @Test
    public void deleteReimbursementAmountAlteration() {
        ReimbursementAmountAlteration ra = rr.addReimbursementAmountAlteration(new ReimbursementAmountAlteration(44,44444,44444,"Test",r.getRequest(41),er.getEmployee(4)));
        ReimbursementAmountAlteration deletedR = rr.deleteReimbursementAmountAlteration(ra.getId());
        assertEquals(ra, deletedR);
    }
}
