package com.revature.unittesting.DAO;

import com.revature.models.RequestStatuses;
import com.revature.repo.RequestStatusesRepo;
import com.revature.repo.RequestStatusesRepoImpl;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RequestStatusDAOTesting {
    RequestStatusesRepo rsr = new RequestStatusesRepoImpl();

    @Test
    public void getRequestStatuse (){
        RequestStatuses expected = new RequestStatuses(1,"pending employee action");
        RequestStatuses actual = rsr.getRequestStatus(1);
        assertEquals(expected,actual);
    }
    @Test
    public void addRequestStatuse(){
        RequestStatuses expected = new RequestStatuses(44,"Test");
        RequestStatuses actual = new RequestStatuses(44,"Test");
        actual = rsr.addRequestStatus(actual);
        expected.setId(actual.getId());
        assertEquals(actual,expected);
        rsr.deleteRequestStatus(actual.getId());

    }
    @Test
    public void getAllRequestStatuses(){
        List<RequestStatuses> RequestStatusesList = rsr.getAllRequestStatuses();
        assertTrue(RequestStatusesList.size()>0);
    }
    @Test
    public void updateRequestStatuse () {
        RequestStatuses r = rsr.addRequestStatus(new RequestStatuses(44,"Test"));
        r.setStatus("Tested");
        RequestStatuses updatedR = rsr.updateRequestStatus(r);
        assertEquals(r,updatedR);
        rsr.deleteRequestStatus(r.getId());
    }
    @Test
    public void deleteRequestStatuse() {
        RequestStatuses r = rsr.addRequestStatus(new RequestStatuses(44, "Test"));
        RequestStatuses deletedR = rsr.deleteRequestStatus(r.getId());
        assertEquals(r, deletedR);
    }
}
