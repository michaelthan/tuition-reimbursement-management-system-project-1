package com.revature.unittesting.DAO;

import com.revature.models.Departments;
import com.revature.repo.DepartmentsRepo;
import com.revature.repo.DepartmentsRepoImpl;
import org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class DepartmentsDAOTesting {
    DepartmentsRepo dr = new DepartmentsRepoImpl();
    @Test
    public void getDeparment(){
        Departments expected = new Departments(5,"General");
        Departments actual = dr.getDepartment(5);
        assertEquals(expected,actual);
    }
    @Test
    public void addDepartments(){
        Departments expected = new Departments(17,"Test");
        Departments actual = new Departments(17,"Test");
        actual = dr.addDepartment(actual);
        expected.setId(actual.getId());
        assertEquals(expected, actual);
        dr.deleteDepartment(actual.getId());
    }
    @Test
    public void getAllDepartments(){
        List<Departments> departmentsList = dr.getAllDepartments();
        assertTrue(departmentsList.size()>0);
    }
    @Test
    public void updateDepartments(){
        Departments d = dr.addDepartment(new Departments(17, "Test"));
        d.setDepartment("Tested");
        Departments updatedD = dr.updateDepartment(d);
        assertEquals(d, updatedD);
        dr.deleteDepartment(d.getId());
    }
    @Test
    public void deleteDepartments(){
        Departments d =  dr.addDepartment(new Departments(17, "Test"));
        Departments deletedD = dr.deleteDepartment(d.getId());
        assertEquals(d,deletedD);
    }
}
