package com.revature.unittesting.DAO;

import com.revature.models.TuitionTypes;
import com.revature.repo.TuitionTypesRepo;
import com.revature.repo.TuitionTypesRepoImpl;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TuitionTypesDAOTesting {
    TuitionTypesRepo tr = new TuitionTypesRepoImpl();

    @Test
    public void getTuitionType (){
        TuitionTypes expected = new TuitionTypes(1,"University Course", .8);
        TuitionTypes actual = tr.getTuitionTypes(1);
        assertEquals(expected,actual);
    }
    @Test
    public void addTuitionType(){
        TuitionTypes expected = new TuitionTypes(44,"Test",4.44);
        TuitionTypes actual = new TuitionTypes(44,"Test",4.44);
        actual = tr.addTuitionType(actual);
        expected.setId(actual.getId());
        assertEquals(actual,expected);
        tr.deleteTuitionType(actual.getId());

    }
    @Test
    public void getAllTuitionTypes(){
        List<TuitionTypes> TuitionTypesList = tr.getAllTuitionTypes();
        assertTrue(TuitionTypesList.size()>0);
    }
    @Test
    public void updateTuitionType () {
        TuitionTypes r = tr.addTuitionType(new TuitionTypes(44,"Test",4.44));
        r.setTuition("Tested");
        TuitionTypes updatedR = tr.updateTuitionType(r);
        assertEquals(r,updatedR);
        tr.deleteTuitionType(r.getId());
    }
    @Test
    public void deleteTuitionType() {
        TuitionTypes r = tr.addTuitionType(new TuitionTypes(44,"Test",4.44));
        TuitionTypes deletedR = tr.deleteTuitionType(r.getId());
        assertEquals(r, deletedR);
    }
}
