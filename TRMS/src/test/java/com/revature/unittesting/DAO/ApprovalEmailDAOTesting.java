package com.revature.unittesting.DAO;

import com.revature.app.App;
import com.revature.models.ApprovalEmail;
import com.revature.repo.ApprovalEmailRepo;
import com.revature.repo.ApprovalEmailRepoImpl;
import com.revature.repo.EmployeeRepo;
import com.revature.repo.EmployeeRepoImpl;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ApprovalEmailDAOTesting {
    ApprovalEmailRepo ar = new ApprovalEmailRepoImpl();
    EmployeeRepo er = new EmployeeRepoImpl();
    @Test
    public void getApprovalEmail (){
        ApprovalEmail expected = new ApprovalEmail(1,"Test",er.getEmployee(3));
        ApprovalEmail actual = ar.addApprovalEmail(expected);
        actual = ar.getApprovalEmail(actual.getId());
        assertEquals(expected,actual);
    }
    @Test
    public void addApprovalEmail(){
        ApprovalEmail expected = new ApprovalEmail(44,"Test", er.getEmployee(3));
        ApprovalEmail actual = new ApprovalEmail(44,"Test", er.getEmployee(3));
        actual = ar.addApprovalEmail(actual);
        expected.setId(actual.getId());
        assertEquals(actual,expected);
        ar.deleteApprovalEmail(actual.getId());

    }
    @Test
    public void getAllApprovalEmail(){
        List<ApprovalEmail> ApprovalEmailList = ar.getAllApprovalEmails();
        assertTrue(ApprovalEmailList.size()>0);
    }
    @Test
    public void updateApprovalEmail () {
        ApprovalEmail r = ar.addApprovalEmail(new ApprovalEmail(44,"Test", er.getEmployee(3)));
        r.setFilePath("Tested");
        ApprovalEmail updatedR = ar.updateApprovalEmail(r);
        assertEquals(r,updatedR);
        ar.deleteApprovalEmail(r.getId());
    }
    @Test
    public void deleteApprovalEmail() {
        ApprovalEmail r = ar.addApprovalEmail(new ApprovalEmail(44,"Test", er.getEmployee(3)));
        System.out.println(r.getId());
        ApprovalEmail deletedR = ar.deleteApprovalEmail(r.getId());
        assertEquals(r, deletedR);
    }
}
