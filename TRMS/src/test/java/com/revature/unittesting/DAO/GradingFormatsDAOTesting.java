package com.revature.unittesting.DAO;

import com.revature.models.GradingFormats;
import com.revature.repo.GradingFormatsRepo;
import com.revature.repo.GradingFormatsRepoImpl;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class GradingFormatsDAOTesting {
    GradingFormatsRepo gr = new GradingFormatsRepoImpl();
    @Test
    public void getGradingFormat (){
        GradingFormats expected = new GradingFormats(1,"Letter Grade","C");
        GradingFormats actual = gr.getGradingFormat(1);
        assertEquals(expected,actual);
    }
    @Test
    public void addGradingFormat(){
        GradingFormats expected = new GradingFormats(1,"Test","AAAA");
        GradingFormats actual = new GradingFormats(1,"Test","AAAA");
        actual = gr.addGradingFormat(actual);
        expected.setId(actual.getId());
        assertEquals(actual,expected);
        gr.deleteGradingFormat(actual.getId());

    }
    @Test
    public void getAllGradingFormats(){
        List<GradingFormats> GradingFormatsList = gr.getAllGradingFormats();
        assertTrue(GradingFormatsList.size()>0);
    }
    @Test
    public void updateGradingFormat () {
        GradingFormats r = gr.addGradingFormat(new GradingFormats(1,"Test","AAAA"));
        r.setFormat("Tested");
        GradingFormats updatedR = gr.updateGradingFormat(r);
        assertEquals(r,updatedR);
        gr.deleteGradingFormat(r.getId());
    }
    @Test
    public void deleteGradingFormat() {
        GradingFormats r = gr.addGradingFormat(new GradingFormats(1,"Test","AAAA"));
        GradingFormats deletedR = gr.deleteGradingFormat(r.getId());
        assertEquals(r, deletedR);
    }
}
