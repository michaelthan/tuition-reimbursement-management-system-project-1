package com.revature.unittesting.services;

import com.revature.models.Employee;
import com.revature.models.GradePresentation;
import com.revature.models.Request;
import com.revature.repo.*;
import com.revature.service.GradePresentationService;
import com.revature.service.GradePresentationServiceImpl;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class GradePresentationRepoTesting {
    GradePresentationRepo gpr = new GradePresentationRepoImpl();
    GradePresentationService gps = new GradePresentationServiceImpl(gpr);
    RequestRepo rr = new RequestRepoImpl();
    EmployeeRepo er = new EmployeeRepoImpl();

    @Test
    public void getGradePresentation() {
        GradePresentation expected = new GradePresentation(36,"test", false, true, rr.getRequest(41), er.getEmployee(1));
        GradePresentation actual = gps.getGradePresentation(36);
        assertEquals(expected,actual);
    }
    @Test
    public void addGradePresentation(){
        GradePresentation expected = new GradePresentation(44,"test", false, false, rr.getRequest(41), er.getEmployee(1));
        GradePresentation actual = new GradePresentation(44,"test", false, false, rr.getRequest(41), er.getEmployee(1));
        actual = gps.addGradePresentation(actual);
        expected.setId(actual.getId());
        assertEquals(expected, actual);
        gps.deleteGradePresentation(actual.getId());
    }
    @Test
    public void getAllGradePresentation() {
        List<GradePresentation> GradePresentationList = gps.getAllGradePresentations();
        assertTrue(GradePresentationList.size()>0);
    }
    @Test
    public void updateGradePresentation(){
        GradePresentation d = gps.addGradePresentation(new GradePresentation(44,"test", false, false, rr.getRequest(41), er.getEmployee(1)));
        d.setFilePath("Tested");
        GradePresentation updatedD = gps.updateGradePresentation(d);
        assertEquals(d, updatedD);
        gps.deleteGradePresentation(d.getId());
    }
    @Test
    public void deleteGradePresentation(){
        GradePresentation d =  gps.addGradePresentation(new GradePresentation(44,"test", false, false, rr.getRequest(41), er.getEmployee(1)));
        GradePresentation deletedD = gps.deleteGradePresentation(d.getId());
        assertEquals(d,deletedD);
    }
}
