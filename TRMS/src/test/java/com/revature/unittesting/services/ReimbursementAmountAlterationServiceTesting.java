package com.revature.unittesting.services;

import com.revature.models.ReimbursementAmountAlteration;
import com.revature.repo.*;
import com.revature.service.ReimbursementAmountAlterationService;
import com.revature.service.ReimbursementAmountAlterationServiceImpl;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ReimbursementAmountAlterationServiceTesting {
    ReimbursementAmountAlterationRepo rar = new ReimbursementAmountAlterationRepoImpl();
    ReimbursementAmountAlterationService ras = new ReimbursementAmountAlterationServiceImpl(rar);
    RequestRepo r = new RequestRepoImpl();
    EmployeeRepo er = new EmployeeRepoImpl();

    @Test
    public void getReimbursementAmountAlteration() {
        ReimbursementAmountAlteration expected = new ReimbursementAmountAlteration(1,200,444,"because",r.getRequest(41),er.getEmployee(4));
        ReimbursementAmountAlteration actual = ras.getReimbursementAmountAlteration(1);
        assertEquals(expected,actual);
    }
    @Test
    public void addReimbursementAmountAlteration(){
        ReimbursementAmountAlteration expected = new ReimbursementAmountAlteration(44,44444,44444,"Test",r.getRequest(41),er.getEmployee(4));
        ReimbursementAmountAlteration actual = new ReimbursementAmountAlteration(44,44444,44444,"Test",r.getRequest(41),er.getEmployee(4));
        actual = ras.addReimbursementAmountAlteration(actual);
        expected.setId(actual.getId());
        assertEquals(expected, actual);
        ras.deleteReimbursementAmountAlteration(actual.getId());
    }
    @Test
    public void getAllReimbursementAmountAlteration() {
        List<ReimbursementAmountAlteration> ReimbursementAmountAlterationList = ras.getAllReimbursementAmountAlterations();
        assertTrue(ReimbursementAmountAlterationList.size()>0);
    }
    @Test
    public void updateReimbursementAmountAlteration(){
        ReimbursementAmountAlteration d = ras.addReimbursementAmountAlteration(new ReimbursementAmountAlteration(44,44444,44444,"Test",r.getRequest(41),er.getEmployee(4)));
        d.setReason("Tested");
        ReimbursementAmountAlteration updatedD = ras.updateReimbursementAmountAlteration(d);
        assertEquals(d, updatedD);
        ras.deleteReimbursementAmountAlteration(d.getId());
    }
    @Test
    public void deleteReimbursementAmountAlteration(){
        ReimbursementAmountAlteration d =  ras.addReimbursementAmountAlteration(new ReimbursementAmountAlteration(44,44444,44444,"Test",r.getRequest(41),er.getEmployee(4)));
        ReimbursementAmountAlteration deletedD = ras.deleteReimbursementAmountAlteration(d.getId());
        assertEquals(d,deletedD);
    }
}
