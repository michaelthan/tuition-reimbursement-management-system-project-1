package com.revature.unittesting.services;


import com.revature.models.Departments;
import com.revature.models.Employee;
import com.revature.repo.*;
import com.revature.service.EmployeeService;
import com.revature.service.EmployeeServiceImpl;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EmployeeServicesTesting {
    EmployeeRepo er = new EmployeeRepoImpl();
    EmployeeService es = new EmployeeServiceImpl(er);
    DepartmentsRepo dr = new DepartmentsRepoImpl();
    RolesRepo rr = new RolesRepoImpl();
    @Test
    public void getEmployee() {
        Employee expected = new Employee(3,"Kel","Smith","kel.smith","apassword",dr.getDepartment(5),rr.getRoles(4),null);
        Employee actual = es.getEmployee(3);
        assertEquals(expected,actual);
    }
    @Test
    public void addEmployee(){
        Employee expected = new Employee(44,"test","Smith","test.smith","testpass",dr.getDepartment(5),rr.getRoles(4),es.getEmployee(1));
        Employee actual = new Employee(44,"test","Smith","test.smith","testpass",dr.getDepartment(5),rr.getRoles(4),es.getEmployee(1));
        actual = es.addEmployee(actual);
        expected.setId(actual.getId());
        assertEquals(expected, actual);
        es.deleteEmployee(actual.getId());
    }
    @Test
    public void getAllEmployee() {
        List<Employee> EmployeeList = es.getAllEmployees();
        assertTrue(EmployeeList.size()>0);
    }
    @Test
    public void updateEmployee(){
        Employee d = es.addEmployee(new Employee(44,"test","Smith","test.smith","testpass",dr.getDepartment(5),rr.getRoles(4),es.getEmployee(1)));
        d.setUsername("Tested");
        Employee updatedD = es.updateEmployee(d);
        assertEquals(d, updatedD);
        es.deleteEmployee(d.getId());
    }
    @Test
    public void deleteEmployee(){
        Employee d =  es.addEmployee(new Employee(44,"test","Smith","test.smith","testpass",dr.getDepartment(5),rr.getRoles(4),es.getEmployee(1)));
        Employee deletedD = es.deleteEmployee(d.getId());
        assertEquals(d,deletedD);
    }
    @Test
    public void login(){
        String username = "kel.smith";
        String password = "apassword";
        assertTrue(es.loginEmployee(username,password));
    }
    @Test
    public void getEmployeeByUsername(){
        Employee e = es.getEmployee(1);
        Employee actualE = es.getEmployeeByUsername(e.getUsername());
        assertEquals(e,actualE);
    }
}
