package com.revature.unittesting.services;

import com.revature.models.ApprovalEmail;
import com.revature.models.Employee;
import com.revature.repo.ApprovalEmailRepo;
import com.revature.repo.ApprovalEmailRepoImpl;
import com.revature.repo.EmployeeRepo;
import com.revature.repo.EmployeeRepoImpl;
import com.revature.service.ApprovalEmailService;
import com.revature.service.ApprovalEmailServiceImpl;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ApprovalEmailDAOTesting {
    ApprovalEmailRepo aer = new ApprovalEmailRepoImpl();
    ApprovalEmailService aes = new ApprovalEmailServiceImpl(aer);
    EmployeeRepo er = new EmployeeRepoImpl();

    @Test
    public void getApprovalEmail() {
        ApprovalEmail expected = new ApprovalEmail(47,"defaultpath", er.getEmployee(3));
        ApprovalEmail actual = aes.getApprovalEmail(47);
        assertEquals(expected,actual);
    }
    @Test
    public void addApprovalEmail(){
        ApprovalEmail expected = new ApprovalEmail(44,"Test",er.getEmployee(3));
        ApprovalEmail actual = new ApprovalEmail(44,"Test",er.getEmployee(3));
        actual = aes.addApprovalEmail(actual);
        expected.setId(actual.getId());
        assertEquals(expected, actual);
        aes.deleteApprovalEmail(actual.getId());
    }
    @Test
    public void getAllApprovalEmail() {
        List<ApprovalEmail> ApprovalEmailList = aes.getAllApprovalEmails();
        assertTrue(ApprovalEmailList.size()>0);
    }
    @Test
    public void updateApprovalEmail(){
        ApprovalEmail d = aes.addApprovalEmail(new ApprovalEmail(44,"Test",er.getEmployee(3)));
        d.setFilePath("Tested");
        ApprovalEmail updatedD = aes.updateApprovalEmail(d);
        assertEquals(d, updatedD);
        aes.deleteApprovalEmail(d.getId());
    }
    @Test
    public void deleteApprovalEmail(){
        ApprovalEmail d =  aes.addApprovalEmail(new ApprovalEmail(44,"Test",er.getEmployee(3)));
        ApprovalEmail deletedD = aes.deleteApprovalEmail(d.getId());
        assertEquals(d,deletedD);
    }

    
}
