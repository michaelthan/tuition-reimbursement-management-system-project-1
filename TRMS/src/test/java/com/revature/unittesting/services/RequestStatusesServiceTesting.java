package com.revature.unittesting.services;

import com.revature.models.RequestStatuses;
import com.revature.repo.RequestStatusesRepo;
import com.revature.repo.RequestStatusesRepoImpl;
import com.revature.service.RequestStatusServiceImpl;
import com.revature.service.RequestStatusesService;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RequestStatusesServiceTesting {
    RequestStatusesRepo rsr = new RequestStatusesRepoImpl();
    RequestStatusesService rss = new RequestStatusServiceImpl(rsr);

    @Test
    public void getRequestStatuses() {
        RequestStatuses expected = new RequestStatuses(1,"pending employee action");
        RequestStatuses actual = rss.getRequestStatus(1);
        assertEquals(expected,actual);
    }
    @Test
    public void addRequestStatuses(){
        RequestStatuses expected = new RequestStatuses(17,"Test");
        RequestStatuses actual = new RequestStatuses(17,"Test");
        actual = rss.addRequestStatus(actual);
        expected.setId(actual.getId());
        assertEquals(expected, actual);
        rss.deleteRequestStatus(actual.getId());
    }
    @Test
    public void getAllRequestStatuses() {
        List<RequestStatuses> RequestStatusesList = rss.getAllRequestStatuses();
        assertTrue(RequestStatusesList.size()>0);
    }
    @Test
    public void updateRequestStatuses(){
        RequestStatuses d = rss.addRequestStatus(new RequestStatuses(17, "Test"));
        d.setStatus("Tested");
        RequestStatuses updatedD = rss.updateRequestStatus(d);
        assertEquals(d, updatedD);
        rss.deleteRequestStatus(d.getId());
    }
    @Test
    public void deleteRequestStatuses(){
        RequestStatuses d =  rss.addRequestStatus(new RequestStatuses(17, "Test"));
        RequestStatuses deletedD = rss.deleteRequestStatus(d.getId());
        assertEquals(d,deletedD);
    }
}
