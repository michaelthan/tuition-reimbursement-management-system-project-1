package com.revature.unittesting.services;

import com.revature.models.Departments;
import com.revature.repo.DepartmentsRepo;
import com.revature.repo.DepartmentsRepoImpl;
import com.revature.service.DepartmentsService;
import com.revature.service.DepartmentsServiceImpl;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DepartmentsServiceTesting {
    DepartmentsRepo drr = new DepartmentsRepoImpl();
    DepartmentsService ds = new DepartmentsServiceImpl(drr);

    @Test
    public void getDepartments() {
        Departments expected = new Departments(5,"General");
        Departments actual = ds.getDepartment(5);
        assertEquals(expected,actual);
    }
    @Test
    public void addDepartments(){
        Departments expected = new Departments(17,"Test");
        Departments actual = new Departments(17,"Test");
        actual = ds.addDepartment(actual);
        expected.setId(actual.getId());
        assertEquals(expected, actual);
        ds.deleteDepartment(actual.getId());
    }
    @Test
    public void getAllDepartments() {
        List<Departments> departmentsList = ds.getAllDepartments();
        assertTrue(departmentsList.size()>0);
    }
    @Test
    public void updateDepartments(){
        Departments d = ds.addDepartment(new Departments(17, "Test"));
        d.setDepartment("Tested");
        Departments updatedD = ds.updateDepartment(d);
        assertEquals(d, updatedD);
        ds.deleteDepartment(d.getId());
    }
    @Test
    public void deleteDepartments(){
        Departments d =  ds.addDepartment(new Departments(17, "Test"));
        Departments deletedD = ds.deleteDepartment(d.getId());
        assertEquals(d,deletedD);
    }
}
