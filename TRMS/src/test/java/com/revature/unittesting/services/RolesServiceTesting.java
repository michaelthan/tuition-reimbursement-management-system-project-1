package com.revature.unittesting.services;

import com.revature.models.Roles;
import com.revature.repo.RolesRepo;
import com.revature.repo.RolesRepoImpl;
import com.revature.service.RolesService;
import com.revature.service.RolesServiceImpl;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RolesServiceTesting {
    RolesRepo rr = new RolesRepoImpl();
    RolesService rs = new RolesServiceImpl(rr);

    @Test
    public void getRoles() {
        Roles expected = new Roles(1,"Employee");
        Roles actual = rs.getRoles(1);
        assertEquals(expected,actual);
    }
    @Test
    public void addRoles(){
        Roles expected = new Roles(17,"Test");
        Roles actual = new Roles(17,"Test");
        actual = rs.addRole(actual);
        expected.setId(actual.getId());
        assertEquals(expected, actual);
        rs.deleteRole(actual.getId());
    }
    @Test
    public void getAllRoles() {
        List<Roles> RolesList = rs.getAllRoles();
        assertTrue(RolesList.size()>0);
    }
    @Test
    public void updateRoles(){
        Roles d = rs.addRole(new Roles(17, "Test"));
        d.setRole("Tested");
        Roles updatedD = rs.updateRole(d);
        assertEquals(d, updatedD);
        rs.deleteRole(d.getId());
    }
    @Test
    public void deleteRoles(){
        Roles d =  rs.addRole(new Roles(17, "Test"));
        Roles deletedD = rs.deleteRole(d.getId());
        assertEquals(d,deletedD);
    }
}
