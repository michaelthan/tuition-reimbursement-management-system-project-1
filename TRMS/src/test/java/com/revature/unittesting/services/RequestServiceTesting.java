package com.revature.unittesting.services;

import com.revature.models.Request;
import com.revature.models.RequestStatuses;
import com.revature.repo.*;
import com.revature.service.RequestService;
import com.revature.service.RequestServiceImpl;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class RequestServiceTesting {
    RequestRepo rr = new RequestRepoImpl();
    RequestService rs = new RequestServiceImpl(rr);
    GradingFormatsRepo gfr = new GradingFormatsRepoImpl();
    TuitionTypesRepo ttr = new TuitionTypesRepoImpl();
    EmployeeRepo er = new EmployeeRepoImpl();
    RequestStatusesRepo rsr = new RequestStatusesRepoImpl();

    @Test
    public void getRequest() {
        Request expected = new Request(41,1637703300000L,200,0, false,"A", "to makes moneys","bitcoin","Pomona, California", gfr.getGradingFormat(1),ttr.getTuitionTypes(2),
                er.getEmployee(6), rsr.getRequestStatus(22),1635194164000L,null, null,0);
        Request actual = rs.getRequest(41);
        assertEquals(expected,actual);
    }
    @Test
    public void addRequest(){
        Request expected = new Request(4444,4444444444444L,444,4, false,"4", "44444","44444","44, 44", gfr.getGradingFormat(1),ttr.getTuitionTypes(2),
                er.getEmployee(6), rsr.getRequestStatus(22),444444444L,null, null,4);
        Request actual = new Request(4444,4444444444444L,444,4, false,"4", "44444","44444","44, 44", gfr.getGradingFormat(1),ttr.getTuitionTypes(2),
                er.getEmployee(6), rsr.getRequestStatus(22),444444444L,null, null,4);
        actual = rs.addRequest(actual);
        expected.setId(actual.getId());
        assertEquals(expected, actual);
        rs.deleteRequest(actual.getId());
    }
    @Test
    public void getAllRequest() {
        List<Request> RequestList = rs.getAllRequests();
        assertTrue(RequestList.size()>0);
    }
    @Test
    public void updateRequest(){
        Request d = rs.addRequest(new Request(4444,4444444444444L,444,4, false,"4", "44444","44444","44, 44", gfr.getGradingFormat(1),ttr.getTuitionTypes(2),
                er.getEmployee(6), rsr.getRequestStatus(22),444444444L,null, null,4));
        d.setDescription("Tested");
        Request updatedD = rs.updateRequest(d);
        assertEquals(d, updatedD);
        rs.deleteRequest(d.getId());
    }
    @Test
    public void deleteRequest(){
        Request d =  rs.addRequest(new Request(4444,4444444444444L,444,4, false,"4", "44444","44444","44, 44", gfr.getGradingFormat(1),ttr.getTuitionTypes(2),
                er.getEmployee(6), rsr.getRequestStatus(22),444444444L,null, null,4));
        Request deletedD = rs.deleteRequest(d.getId());
        assertEquals(d,deletedD);
    }
    @Test
    public void getRequestByUsername(){
        String username = "jdaniel";
        List<Request> requestList = rs.getRequestsByUsername(username);
        for (Request r: requestList) {
            assertEquals(username, r.getEmployee().getUsername());
        }
    }
    @Test
    public void getRequestToHandle() {
        String username = "mike.yak";
        RequestStatuses rss = rsr.getRequestStatus(6);
        List<Request> requestList = rs.getRequestsToHandle(username);
        for(Request r: requestList) {
            assertEquals(rss,r.getStatus());
        }
    }
    @Test
    public void requestAction(){
        Request r = new Request(4444,4444444444444L,444,4, false,"4", "44444","44444","44, 44", gfr.getGradingFormat(1),ttr.getTuitionTypes(2),
                er.getEmployee(6), rsr.getRequestStatus(2),444444444L,null, null,4);
        r = rs.addRequest(r);
        rs.requestAction("approve", r.getId());
        assertEquals(rs.getRequest(r.getId()).getStatus(),rsr.getRequestStatus(4));
        rs.requestAction("deny", r.getId());
        assertEquals(rs.getRequest(r.getId()).getStatus(),rsr.getRequestStatus(11));
        r.setStatus(rsr.getRequestStatus(2));
        r = rs.updateRequest(r);
        rs.requestAction("requestedaction",r.getId());
        assertEquals(rs.getRequest(r.getId()).getStatus(),rsr.getRequestStatus(3));
        rs.deleteRequest(r.getId());
    }
}
