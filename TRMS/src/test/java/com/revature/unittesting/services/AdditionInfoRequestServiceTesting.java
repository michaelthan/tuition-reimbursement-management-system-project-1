package com.revature.unittesting.services;

import com.revature.models.AdditionalInfoRequest;
import com.revature.repo.AdditionalInfoRequestRepo;
import com.revature.repo.AdditionalInfoRequestRepoImpl;
import com.revature.repo.RequestRepo;
import com.revature.repo.RequestRepoImpl;
import com.revature.service.AdditionalInfoRequestService;
import com.revature.service.AdditionalInfoRequestServiceImpl;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AdditionInfoRequestServiceTesting {
    AdditionalInfoRequestRepo arr = new AdditionalInfoRequestRepoImpl();
    AdditionalInfoRequestService ars = new AdditionalInfoRequestServiceImpl(arr);

    RequestRepo rr = new RequestRepoImpl();

    @Test
    public void getAdditionalInfoRequest() {
        AdditionalInfoRequest expected = new AdditionalInfoRequest(1,"testing",rr.getRequest(41));
        AdditionalInfoRequest actual = ars.getAdditionalInfoRequest(1);
        actual.initRequest();
        assertEquals(expected,actual);
    }
    @Test
    public void addAdditionalInfoRequest(){
        AdditionalInfoRequest expected = new AdditionalInfoRequest(44,"Test",rr.getRequest(4));
        AdditionalInfoRequest actual = new AdditionalInfoRequest(44,"Test",rr.getRequest(4));
        actual = ars.addAdditionalInfoRequest(actual);
        expected.setId(actual.getId());
        assertEquals(expected, actual);
        ars.deleteAdditionalInfoRequest(actual.getId());
    }
    @Test
    public void getAllAdditionalInfoRequest() {
        List<AdditionalInfoRequest> AdditionalInfoRequestList = ars.getAllAdditionalInfoRequests();
        assertTrue(AdditionalInfoRequestList.size()>0);
    }
    @Test
    public void updateAdditionalInfoRequest(){
        AdditionalInfoRequest d = ars.addAdditionalInfoRequest(new AdditionalInfoRequest(44,"Test",rr.getRequest(4)));
        d.setFilePath("Tested");
        AdditionalInfoRequest updatedD = ars.updateAdditionalInfoRequest(d);
        assertEquals(d, updatedD);
        ars.deleteAdditionalInfoRequest(d.getId());
    }
    @Test
    public void deleteAdditionalInfoRequest(){
        AdditionalInfoRequest d =  ars.addAdditionalInfoRequest(new AdditionalInfoRequest(44,"Test",rr.getRequest(4)));
        AdditionalInfoRequest deletedD = ars.deleteAdditionalInfoRequest(d.getId());
        assertEquals(d,deletedD);
    }
}
