package com.revature.unittesting.services;

import com.revature.models.TuitionTypes;
import com.revature.repo.TuitionTypesRepo;
import com.revature.repo.TuitionTypesRepoImpl;
import com.revature.service.TuitionTypesService;
import com.revature.service.TuitionTypesServiceImpl;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TuitionTypesServiceTesting {
    TuitionTypesRepo ttr = new TuitionTypesRepoImpl();
    TuitionTypesService tts = new TuitionTypesServiceImpl(ttr);

    @Test
    public void getTuitionTypes() {
        TuitionTypes expected = new TuitionTypes(1,"University Course", .8);
        TuitionTypes actual = tts.getTuitionTypes(1);
        assertEquals(expected,actual);
    }
    @Test
    public void addTuitionTypes(){
        TuitionTypes expected = new TuitionTypes(44,"Test",4.44);
        TuitionTypes actual = new TuitionTypes(44,"Test",4.44);
        actual = tts.addTuitionType(actual);
        expected.setId(actual.getId());
        assertEquals(expected, actual);
        tts.deleteTuitionType(actual.getId());
    }
    @Test
    public void getAllTuitionTypes() {
        List<TuitionTypes> TuitionTypesList = tts.getAllTuitionTypes();
        assertTrue(TuitionTypesList.size()>0);
    }
    @Test
    public void updateTuitionTypes(){
        TuitionTypes d = tts.addTuitionType(new TuitionTypes(44,"Test",4.44));
        d.setTuition("Tested");
        TuitionTypes updatedD = tts.updateTuitionType(d);
        assertEquals(d, updatedD);
        tts.deleteTuitionType(d.getId());
    }
    @Test
    public void deleteTuitionTypes(){
        TuitionTypes d =  tts.addTuitionType(new TuitionTypes(44,"Test",4.44));
        TuitionTypes deletedD = tts.deleteTuitionType(d.getId());
        assertEquals(d,deletedD);
    }
    
}
