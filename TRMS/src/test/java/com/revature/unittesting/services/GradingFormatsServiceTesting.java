package com.revature.unittesting.services;

import com.revature.models.GradingFormats;
import com.revature.repo.GradingFormatsRepo;
import com.revature.repo.GradingFormatsRepoImpl;
import com.revature.service.GradingFormatsService;
import com.revature.service.GradingFormatsServiceImpl;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class GradingFormatsServiceTesting {
    GradingFormatsRepo gfr = new GradingFormatsRepoImpl();
    GradingFormatsService gfs = new GradingFormatsServiceImpl(gfr);

    @Test
    public void getGradingFormats() {
        GradingFormats expected = new GradingFormats(1,"Letter Grade","C");
        GradingFormats actual = gfs.getGradingFormat(1);
        assertEquals(expected,actual);
    }
    @Test
    public void addGradingFormats(){
        GradingFormats expected = new GradingFormats(1,"Test","AAAA");
        GradingFormats actual = new GradingFormats(1,"Test","AAAA");
        actual = gfs.addGradingFormat(actual);
        expected.setId(actual.getId());
        assertEquals(expected, actual);
        gfs.deleteGradingFormat(actual.getId());
    }
    @Test
    public void getAllGradingFormats() {
        List<GradingFormats> GradingFormatsList = gfs.getAllGradingFormats();
        assertTrue(GradingFormatsList.size()>0);
    }
    @Test
    public void updateGradingFormats(){
        GradingFormats d = gfs.addGradingFormat(new GradingFormats(1,"Test","AAAA"));
        d.setFormat("Tested");
        GradingFormats updatedD = gfs.updateGradingFormat(d);
        assertEquals(d, updatedD);
        gfs.deleteGradingFormat(d.getId());
    }
    @Test
    public void deleteGradingFormats(){
        GradingFormats d =  gfs.addGradingFormat(new GradingFormats(1,"Test","AAAA"));
        GradingFormats deletedD = gfs.deleteGradingFormat(d.getId());
        assertEquals(d,deletedD);
    }
}
