Feature: Login user

  Scenario: Login user success
    Given the user is on the login page
    When the user logs in with credentials
    Then the user should be on the usersrequests page

  Scenario: Logout user success
    Given the user is on the create request page
    When the user clicks log out
    Then the user is logged out