package com.revature.models;

import com.revature.repo.EmployeeRepo;
import com.revature.repo.EmployeeRepoImpl;
import com.revature.repo.RequestRepo;
import com.revature.repo.RequestRepoImpl;

import javax.persistence.*;
import java.util.NavigableMap;
import java.util.Objects;
@Entity
@Table(name = "reimbursement_amount_alteration")
public class ReimbursementAmountAlteration {
    @Id
    @Column(name = "id", updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    @Column(name = "original", columnDefinition = "numeric(12,2)")
    double original;
    @Column(name = "altered", columnDefinition = "numeric(12,2)")
    double altered;
    @Column(name = "reason")
    String reason;
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @JoinColumn(name = "request")
    Request request;
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @JoinColumn(name = "altered_by")
    Employee alteredBy;

    public ReimbursementAmountAlteration() {
    }

    public ReimbursementAmountAlteration(int id, double original, double altered, String reason, Request request, Employee alteredBy) {
        this.id = id;
        this.original = original;
        this.altered = altered;
        this.reason = reason;
        this.request = request;
        this.alteredBy = alteredBy;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getOriginal() {
        return original;
    }

    public void setOriginal(double original) {
        this.original = original;
    }

    public double getAltered() {
        return altered;
    }

    public void setAltered(double altered) {
        this.altered = altered;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public Employee getAlteredBy() {
        return alteredBy;
    }

    public void setAlteredBy(Employee alteredBy) {
        this.alteredBy = alteredBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ReimbursementAmountAlteration)) return false;
        ReimbursementAmountAlteration that = (ReimbursementAmountAlteration) o;
        this.initRequest();
        this.initAlteredBy();
        that.initRequest();
        that.initAlteredBy();
        return getId() == that.getId() && Double.compare(that.getOriginal(), getOriginal()) == 0 && Double.compare(that.getAltered(), getAltered()) == 0 && Objects.equals(getReason(), that.getReason()) && Objects.equals(getRequest(), that.getRequest()) && Objects.equals(getAlteredBy(), that.getAlteredBy());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getOriginal(), getAltered(), getReason(), getRequest(), getAlteredBy());
    }

    @Override
    public String toString() {
        return "ReimbursementAmountAlteration{" +
                "id=" + id +
                ", original=" + original +
                ", altered=" + altered +
                ", reason='" + reason + '\'' +
                ", request=" + request +
                ", alteredBy=" + alteredBy +
                '}';
    }
    public void initRequest(){
        RequestRepo rr = new RequestRepoImpl();
        this.setRequest(rr.getRequest(request.getId()));
    }
    public void initAlteredBy(){
        EmployeeRepo er = new EmployeeRepoImpl();
        this.setAlteredBy(er.getEmployee(alteredBy.getId()));
    }
}
