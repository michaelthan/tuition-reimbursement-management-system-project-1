package com.revature.models;

import javax.persistence.*;
import java.util.Objects;
@Entity
@Table(name = "grading_formats")
public class GradingFormats {
    @Id
    @Column(name = "id", updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "format")
    String format;
    @Column(name= "default_pass")
    private String defaultPass;

    public GradingFormats(){

    }
    public GradingFormats(int id, String format, String defaultPass) {
        this.id = id;
        this.format = format;
        this.defaultPass = defaultPass;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getDefaultPass() {
        return defaultPass;
    }

    public void setDefaultPass(String defaultPass) {
        this.defaultPass = defaultPass;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GradingFormats)) return false;
        GradingFormats that = (GradingFormats) o;
        return getId() == that.getId() && Objects.equals(getFormat(), that.getFormat()) && Objects.equals(getDefaultPass(), that.getDefaultPass());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getFormat(), getDefaultPass());
    }

    @Override
    public String toString() {
        return "GradingFormats{" +
                "id=" + id +
                ", format='" + format + '\'' +
                ", defaultPass='" + defaultPass + '\'' +
                '}';
    }
}
