package com.revature.models;

import javax.persistence.*;
import java.util.Objects;
@Entity
@Table(name = "request_statuses")
public class RequestStatuses {
    @Id
    @Column(name = "id", updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "status")
    private String status;

    public RequestStatuses(){

    }
    public RequestStatuses(int id, String status) {
        this.id = id;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RequestStatuses)) return false;
        RequestStatuses that = (RequestStatuses) o;
        return getId() == that.getId() && Objects.equals(getStatus(), that.getStatus());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getStatus());
    }

    @Override
    public String toString() {
        return "RequestStatuses{" +
                "id=" + id +
                ", status='" + status + '\'' +
                '}';
    }
}
