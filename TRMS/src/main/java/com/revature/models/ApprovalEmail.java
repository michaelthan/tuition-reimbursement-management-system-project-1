package com.revature.models;

import com.revature.repo.EmployeeRepo;
import com.revature.repo.EmployeeRepoImpl;
import org.hibernate.Hibernate;
import org.hibernate.proxy.HibernateProxy;

import javax.persistence.*;
import java.util.Objects;
@Entity
@Table(name = "approval_email")
public class ApprovalEmail {
    @Id
    @Column(name = "id", updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    @Column(name = "file_path")
    String filePath;
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @JoinColumn(name = "approved_by")
    Employee approvedBy;

    public ApprovalEmail(){

    }
    public ApprovalEmail(int id, String filePath, Employee approvedBy) {
        this.id = id;
        this.filePath = filePath;
        this.approvedBy = approvedBy;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Employee getApprovedBy() {
        if(approvedBy==null) {
            return new Employee();
        }
        EmployeeRepo er = new EmployeeRepoImpl();
        Employee e = er.getEmployee(approvedBy.getId());
        return e;
    }

    public void setApprovedBy(Employee approvedBy) {
        this.approvedBy = approvedBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ApprovalEmail)) return false;
        ApprovalEmail that = (ApprovalEmail) o;
        return getId() == that.getId() && Objects.equals(getFilePath(), that.getFilePath()) && Objects.equals(getApprovedBy(), that.getApprovedBy());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getFilePath(), getApprovedBy());
    }

    @Override
    public String toString() {
        return "ApprovalEmail{" +
                "id=" + id +
                ", filePath='" + filePath + '\'' +
                ", approvedBy=" + approvedBy +
                '}';
    }
}
