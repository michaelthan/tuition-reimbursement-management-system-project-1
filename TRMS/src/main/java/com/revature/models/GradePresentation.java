package com.revature.models;

import com.revature.repo.EmployeeRepo;
import com.revature.repo.EmployeeRepoImpl;
import com.revature.repo.RequestRepo;
import com.revature.repo.RequestRepoImpl;

import javax.persistence.*;
import java.util.Objects;
@Entity
@Table(name = "grade_presentation")
public class GradePresentation {
    @Id
    @Column(name = "id", updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    @Column(name = "file_path")
    String filePath;
    @Column(name = "presentation")
    boolean isPresentation;
    @Column(name = "satisfactory")
    boolean isSatisfactory;
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @JoinColumn(name = "request")
    Request request;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "approver")
    Employee approver;

    public GradePresentation(){

    }
    public GradePresentation(int id, String filePath, boolean isPresentation, boolean isSatisfactory, Request request, Employee approver) {
        this.id = id;
        this.filePath = filePath;
        this.isPresentation = isPresentation;
        this.isSatisfactory = isSatisfactory;
        this.request = request;
        this.approver = approver;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public boolean isPresentation() {
        return isPresentation;
    }

    public void setPresentation(boolean presentation) {
        isPresentation = presentation;
    }

    public boolean isSatisfactory() {
        return isSatisfactory;
    }

    public void setSatisfactory(boolean satisfactory) {
        isSatisfactory = satisfactory;
    }

    public Request getRequest() {
        if(request==null)
            return new Request();
        RequestRepo rr = new RequestRepoImpl();
        return rr.getRequest(this.request.getId());
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public Employee getApprover() {
        EmployeeRepo er = new EmployeeRepoImpl();
        return er.getEmployee(approver.getId());
    }

    public void setApprover(Employee approver) {
        this.approver = approver;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GradePresentation)) return false;
        GradePresentation that = (GradePresentation) o;
        return getId() == that.getId() && isPresentation() == that.isPresentation() && isSatisfactory() == that.isSatisfactory() && Objects.equals(getFilePath(), that.getFilePath()) && Objects.equals(getRequest(), that.getRequest()) && Objects.equals(getApprover(), that.getApprover());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getFilePath(), isPresentation(), isSatisfactory(), getRequest(), getApprover());
    }

    @Override
    public String toString() {
        return "GradePresentation{" +
                "id=" + id +
                ", filePath='" + filePath + '\'' +
                ", isPresentation=" + isPresentation +
                ", isSatisfactory=" + isSatisfactory +
                ", request=" + request +
                ", approver=" + approver +
                '}';
    }
}
