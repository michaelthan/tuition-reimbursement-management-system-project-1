package com.revature.models;

import javax.persistence.*;
import java.util.Objects;
@Entity
@Table (name = "departments")
public class Departments {
    @Id
    @Column(name = "id", updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "department")
    private String department;

    public Departments (){

    }
    public Departments(int id, String department) {
        this.id = id;
        this.department = department;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Departments)) return false;
        Departments that = (Departments) o;
        return getId() == that.getId() && Objects.equals(getDepartment(), that.getDepartment());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getDepartment());
    }

    @Override
    public String toString() {
        return "Departments{" +
                "id=" + id +
                ", department='" + department + '\'' +
                '}';
    }
}
