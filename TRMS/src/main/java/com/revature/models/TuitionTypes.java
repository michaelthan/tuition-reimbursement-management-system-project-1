package com.revature.models;

import javax.persistence.*;
import java.util.Objects;
@Entity
@Table(name = "tuition_types")
public class TuitionTypes {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "tuition")
    private String tuition;
    @Column(name = "reimbursement_amount", columnDefinition = "numeric(12,2)")
    private double reimbursementAmount;

    public TuitionTypes(){

    }
    public TuitionTypes(int id, String tuition, double reimbursementAmount) {
        this.id = id;
        this.tuition = tuition;
        this.reimbursementAmount = reimbursementAmount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTuition() {
        return tuition;
    }

    public void setTuition(String tuition) {
        this.tuition = tuition;
    }

    public double getReimbursementAmount() {
        return reimbursementAmount;
    }

    public void setReimbursementAmount(double reimbursementAmount) {
        this.reimbursementAmount = reimbursementAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TuitionTypes)) return false;
        TuitionTypes that = (TuitionTypes) o;
        return getId() == that.getId() && Double.compare(that.getReimbursementAmount(), getReimbursementAmount()) == 0 && Objects.equals(getTuition(), that.getTuition());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTuition(), getReimbursementAmount());
    }

    @Override
    public String toString() {
        return "TuitionTypes{" +
                "id=" + id +
                ", tuition='" + tuition + '\'' +
                ", reimbursementAmount=" + reimbursementAmount +
                '}';
    }
}
