package com.revature.models;

import com.revature.repo.*;

import javax.persistence.*;
import java.beans.ConstructorProperties;
import java.sql.Date;
import java.sql.Time;
import java.util.Objects;
@Entity
@Table(name = "request")
public class Request {
    @Id
    @Column(name = "id", updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    @Column(name = "request_date", columnDefinition = "numeric")
    Long date;
    @Column(name = "cost" , columnDefinition = "numeric(12,2)")
    double cost;
    @Column(name = "reimbursement_amount" , columnDefinition = "numeric(12,2)")
    double reimbursementAmount;
    @Column(name = "urgent")
    boolean isUrgent;
    @Column(name = "pass_grade")
    String passGrade;
    @Column(name = "work_related_justification")
    String workRelatedJustification;
    @Column(name = "description")
    String description;
    @Column(name = "location")
    String location;
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @JoinColumn(name = "grading_format")
    GradingFormats gradingFormat;
    @ManyToOne(fetch = FetchType.LAZY, cascade =  CascadeType.DETACH)
    @JoinColumn(name = "tuition_type")
    TuitionTypes tuitionType;
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @JoinColumn(name = "employee")
    Employee employee;
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @JoinColumn(name = "status")
    RequestStatuses status;
    @Column(name = "status_time", columnDefinition = "numeric")
    Long statusTime; // change to long soon from big int
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "passing_grade_doc")
    GradePresentation passingGradeDoc;
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "approval_email")
    ApprovalEmail approvalEmail;
    @Column(name = "worktime_missed", columnDefinition = "numeric(12,2)")
    double workTimeMissed;

    public Request() {
    }

    public Request(int id, Long date, double cost, double reimbursementAmount, boolean isUrgent, String passGrade,
                   String workRelatedJustification, String description, String location, GradingFormats gradingFormat, TuitionTypes tuitionType,
                   Employee employee, RequestStatuses status, Long status_time, GradePresentation passingGradeDoc, ApprovalEmail approvalEmail,
                   double workTimeMissed) {
        this.id = id;
        this.date = date;
        this.cost = cost;
        this.reimbursementAmount = reimbursementAmount;
        this.isUrgent = isUrgent;
        this.passGrade = passGrade;
        this.workRelatedJustification = workRelatedJustification;
        this.description = description;
        this.location = location;
        this.gradingFormat = gradingFormat;
        this.tuitionType = tuitionType;
        this.employee = employee;
        this.status = status;
        this.statusTime = status_time;
        this.passingGradeDoc = passingGradeDoc;
        this.approvalEmail = approvalEmail;
        this.workTimeMissed = workTimeMissed;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public double getReimbursementAmount() {
        return reimbursementAmount;
    }

    public void setReimbursementAmount(double reimbursementAmount) {
        this.reimbursementAmount = reimbursementAmount;
    }

    public boolean isUrgent() {
        return isUrgent;
    }

    public void setUrgent(boolean urgent) {
        isUrgent = urgent;
    }

    public String getPassGrade() {
        return passGrade;
    }

    public void setPassGrade(String passGrade) {
        this.passGrade = passGrade;
    }

    public String getWorkRelatedJustification() {
        return workRelatedJustification;
    }

    public void setWorkRelatedJustification(String workRelatedJustification) {
        this.workRelatedJustification = workRelatedJustification;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public GradingFormats getGradingFormat() {
        if(gradingFormat==null)
            return new GradingFormats();
        GradingFormatsRepo gfr = new GradingFormatsRepoImpl();
        return gfr.getGradingFormat(gradingFormat.getId());
    }

    public void setGradingFormat(GradingFormats gradingFormat) {
        this.gradingFormat = gradingFormat;
    }

    public TuitionTypes getTuitionType() {
        if(tuitionType==null)
            return new TuitionTypes();
        TuitionTypesRepo ttr = new TuitionTypesRepoImpl();
        return ttr.getTuitionTypes(tuitionType.getId());
    }

    public void setTuitionType(TuitionTypes tuitionType) {
        this.tuitionType = tuitionType;
    }

    public Employee getEmployee() {
        if(employee==null)
            return new Employee();
        EmployeeRepo er = new EmployeeRepoImpl();
        return er.getEmployee(employee.getId());
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public RequestStatuses getStatus() {
        if(status==null)
            return new RequestStatuses();
        RequestStatusesRepo rsr = new RequestStatusesRepoImpl();
        return rsr.getRequestStatus(status.getId());
    }

    public void setStatus(RequestStatuses status) {
        this.status = status;
    }

    public Long getStatus_time() {
        return statusTime;
    }

    public void setStatus_time(Long status_time) {
        this.statusTime = status_time;
    }

    public GradePresentation getPassingGradeDoc() {
        GradePresentationRepo gpr = new GradePresentationRepoImpl();
        if(passingGradeDoc==null)
            return new GradePresentation();
        GradePresentation gp = gpr.getGradePresentation(passingGradeDoc.getId());
        return gp;
    }

    public void setPassingGradeDoc(GradePresentation passingGradeDoc) {
        this.passingGradeDoc = passingGradeDoc;
    }

    public ApprovalEmail getApprovalEmail() {
        if(approvalEmail==null)
            return new ApprovalEmail();
        ApprovalEmailRepo aer = new ApprovalEmailRepoImpl();
        return aer.getApprovalEmail(approvalEmail.getId());
    }

    public void setApprovalEmail(ApprovalEmail approvalEmail) {
        this.approvalEmail = approvalEmail;
    }

    public double getWorkTimeMissed() {
        return workTimeMissed;
    }

    public void setWorkTimeMissed(double workTimeMissed) {
        this.workTimeMissed = workTimeMissed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Request)) return false;
        Request request = (Request) o;
        return getId() == request.getId() && Double.compare(request.getCost(), getCost()) == 0 && Double.compare(request.getReimbursementAmount(), getReimbursementAmount()) == 0 && isUrgent() == request.isUrgent() && Double.compare(request.getWorkTimeMissed(), getWorkTimeMissed()) == 0 && Objects.equals(getDate(), request.getDate()) && Objects.equals(getPassGrade(), request.getPassGrade()) && Objects.equals(getWorkRelatedJustification(), request.getWorkRelatedJustification()) && Objects.equals(getDescription(), request.getDescription()) && Objects.equals(getLocation(), request.getLocation()) && Objects.equals(getGradingFormat(), request.getGradingFormat()) && Objects.equals(getTuitionType(), request.getTuitionType()) && Objects.equals(getEmployee(), request.getEmployee()) && Objects.equals(getStatus(), request.getStatus()) && Objects.equals(statusTime, request.statusTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getDate(), getCost(), getReimbursementAmount(), isUrgent(), getPassGrade(), getWorkRelatedJustification(), getDescription(), getLocation(), getGradingFormat(), getTuitionType(), getEmployee(), getStatus(), statusTime, getPassingGradeDoc(), getApprovalEmail(), getWorkTimeMissed());
    }

    @Override
    public String toString() {
        return "Request{" +
                "id=" + id +
                ", date=" + date +
                ", cost=" + cost +
                ", reimbursementAmount=" + reimbursementAmount +
                ", isUrgent=" + isUrgent +
                ", passGrade='" + passGrade + '\'' +
                ", workRelatedJustification='" + workRelatedJustification + '\'' +
                ", description='" + description + '\'' +
                ", location='" + location + '\'' +
                ", gradingFormat=" + gradingFormat +
                ", tuitionType=" + tuitionType +
                ", employee=" + employee +
                ", status=" + status +
                ", statusTime=" + statusTime +
                ", passingGradeDoc=" + passingGradeDoc +
                ", approvalEmail=" + approvalEmail +
                ", workTimeMissed=" + workTimeMissed +
                '}';
    }
}
