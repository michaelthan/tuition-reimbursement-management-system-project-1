package com.revature.models;

import com.revature.repo.RequestRepo;
import com.revature.repo.RequestRepoImpl;

import javax.persistence.*;
import java.util.Objects;
@Entity
@Table(name = "additional_info_request")
public class AdditionalInfoRequest {
    @Id
    @Column(name = "id", updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    @Column(name = "file_path")
    String filePath;
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn (name="request")
    Request request;

    public AdditionalInfoRequest() {
    }

    public AdditionalInfoRequest(int id, String filePath, Request request) {
        this.id = id;
        this.filePath = filePath;
        this.request = request;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AdditionalInfoRequest)) return false;
        AdditionalInfoRequest that = (AdditionalInfoRequest) o;
        return getId() == that.getId() && Objects.equals(getFilePath(), that.getFilePath()) && Objects.equals(getRequest(), that.getRequest());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getFilePath(), getRequest());
    }

    @Override
    public String toString() {
        return "AdditionalnfoRequest{" +
                "id=" + id +
                ", filePath='" + filePath + '\'' +
                ", request=" + request +
                '}';
    }
    public void initRequest(){
        RequestRepo rr = new RequestRepoImpl();
        this.setRequest(rr.getRequest(request.getId()));
    }
}
