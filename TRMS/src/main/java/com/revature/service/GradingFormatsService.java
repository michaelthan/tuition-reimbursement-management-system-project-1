package com.revature.service;

import com.revature.models.GradingFormats;

import java.util.List;

public interface GradingFormatsService {
    public GradingFormats getGradingFormat(int id);
    public List<GradingFormats> getAllGradingFormats();
    public GradingFormats addGradingFormat(GradingFormats g);
    public GradingFormats updateGradingFormat(GradingFormats change);
    public GradingFormats deleteGradingFormat(int id);
}
