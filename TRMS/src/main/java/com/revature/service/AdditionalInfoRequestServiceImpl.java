package com.revature.service;

import com.revature.models.AdditionalInfoRequest;
import com.revature.repo.AdditionalInfoRequestRepo;

import java.util.List;

public class AdditionalInfoRequestServiceImpl implements AdditionalInfoRequestService{
    AdditionalInfoRequestRepo ar;
    public AdditionalInfoRequestServiceImpl (AdditionalInfoRequestRepo ar) {
        this.ar = ar;
    }
    @Override
    public AdditionalInfoRequest getAdditionalInfoRequest(int id) {
        return ar.getAdditionalInfoRequest(id);
    }

    @Override
    public List<AdditionalInfoRequest> getAllAdditionalInfoRequests() {
        return ar.getAllAdditionalInfoRequests();
    }

    @Override
    public AdditionalInfoRequest addAdditionalInfoRequest(AdditionalInfoRequest a) {
        return ar.addAdditionalInfoRequest(a);
    }

    @Override
    public AdditionalInfoRequest updateAdditionalInfoRequest(AdditionalInfoRequest change) {
        return ar.updateAdditionalInfoRequest(change);
    }

    @Override
    public AdditionalInfoRequest deleteAdditionalInfoRequest(int id) {
        return ar.deleteAdditionalInfoRequest(id);
    }
}
