package com.revature.service;

import com.revature.models.Roles;
import com.revature.repo.RolesRepo;

import java.util.List;

public class RolesServiceImpl implements RolesService{
    RolesRepo rr;
    public RolesServiceImpl(RolesRepo rr) {
        this.rr = rr;
    }
    @Override
    public Roles getRoles(int id) {
        return rr.getRoles(id);
    }

    @Override
    public List<Roles> getAllRoles() {
        return rr.getAllRoles();
    }

    @Override
    public Roles addRole(Roles r) {
        return rr.addRole(r);
    }

    @Override
    public Roles updateRole(Roles change) {
        return rr.updateRole(change);
    }

    @Override
    public Roles deleteRole(int id) {
        return rr.deleteRole(id);
    }
}
