package com.revature.service;

import com.revature.models.TuitionTypes;
import com.revature.repo.TuitionTypesRepo;

import java.util.List;

public class TuitionTypesServiceImpl implements  TuitionTypesService{
    TuitionTypesRepo tr;
    public TuitionTypesServiceImpl (TuitionTypesRepo tr) {
        this.tr = tr;
    }
    @Override
    public TuitionTypes getTuitionTypes(int id) {
        return tr.getTuitionTypes(id);
    }

    @Override
    public List<TuitionTypes> getAllTuitionTypes() {
        return tr.getAllTuitionTypes();
    }

    @Override
    public TuitionTypes addTuitionType(TuitionTypes t) {
        return tr.addTuitionType(t);
    }

    @Override
    public TuitionTypes updateTuitionType(TuitionTypes change) {
        return tr.updateTuitionType(change);
    }

    @Override
    public TuitionTypes deleteTuitionType(int id) {
        return tr.deleteTuitionType(id);
    }
}
