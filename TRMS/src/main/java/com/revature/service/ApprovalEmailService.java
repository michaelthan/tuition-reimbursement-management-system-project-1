package com.revature.service;

import com.revature.models.ApprovalEmail;

import java.util.List;

public interface ApprovalEmailService {
    public ApprovalEmail getApprovalEmail(int id);
    public List<ApprovalEmail> getAllApprovalEmails();
    public ApprovalEmail addApprovalEmail(ApprovalEmail a);
    public ApprovalEmail updateApprovalEmail(ApprovalEmail a);
    public ApprovalEmail deleteApprovalEmail(int id);
}
