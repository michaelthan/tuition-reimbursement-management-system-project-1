package com.revature.service;

import com.revature.models.GradePresentation;
import com.revature.repo.GradePresentationRepo;

import java.util.List;

public class GradePresentationServiceImpl implements GradePresentationService{
    GradePresentationRepo gr;

    public GradePresentationServiceImpl (GradePresentationRepo gr) {
        this.gr = gr;
    }
    @Override
    public GradePresentation getGradePresentation(int id) {
        return gr.getGradePresentation(id);
    }

    @Override
    public List<GradePresentation> getAllGradePresentations() {
        return gr.getAllGradePresentations();
    }

    @Override
    public GradePresentation addGradePresentation(GradePresentation g) {
        return gr.addGradePresentation(g);
    }

    @Override
    public GradePresentation updateGradePresentation(GradePresentation change) {
        return gr.updateGradePresentation(change);
    }

    @Override
    public GradePresentation deleteGradePresentation(int id) {
        return gr.deleteGradePresentation(id);
    }
}
