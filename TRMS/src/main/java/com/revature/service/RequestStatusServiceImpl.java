package com.revature.service;

import com.revature.models.RequestStatuses;
import com.revature.repo.RequestStatusesRepo;

import java.util.List;

public class RequestStatusServiceImpl implements RequestStatusesService{
    RequestStatusesRepo rr;
    public RequestStatusServiceImpl (RequestStatusesRepo rr) {
        this.rr = rr;
    }
    @Override
    public RequestStatuses getRequestStatus(int id) {
        return rr.getRequestStatus(id);
    }

    @Override
    public List<RequestStatuses> getAllRequestStatuses() {
        return rr.getAllRequestStatuses();
    }

    @Override
    public RequestStatuses addRequestStatus(RequestStatuses r) {
        return rr.addRequestStatus(r);
    }

    @Override
    public RequestStatuses updateRequestStatus(RequestStatuses change) {
        return rr.updateRequestStatus(change);
    }

    @Override
    public RequestStatuses deleteRequestStatus(int id) {
        return rr.deleteRequestStatus(id);
    }
}
