package com.revature.service;

import com.revature.models.Departments;
import com.revature.repo.DepartmentsRepo;

import java.util.List;

public class DepartmentsServiceImpl implements DepartmentsService{
    DepartmentsRepo dr;

    public DepartmentsServiceImpl(DepartmentsRepo dr) {
        this.dr =dr;
    }

    @Override
    public Departments getDepartment(int id) {
        return dr.getDepartment(id);
    }

    @Override
    public List<Departments> getAllDepartments() {
        return dr.getAllDepartments();
    }

    @Override
    public Departments addDepartment(Departments d) {
        return dr.addDepartment(d);
    }

    @Override
    public Departments updateDepartment(Departments d) {
        return dr.updateDepartment(d);
    }

    @Override
    public Departments deleteDepartment(int id) {
        return dr.deleteDepartment(id);
    }
}
