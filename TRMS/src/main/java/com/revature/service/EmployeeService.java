package com.revature.service;

import com.revature.models.Employee;

import java.util.List;

public interface EmployeeService {
    public Employee getEmployee (int id);
    public List<Employee> getAllEmployees();
    public Employee addEmployee(Employee e);
    public Employee updateEmployee(Employee e);
    public Employee deleteEmployee(int id);

    public Boolean loginEmployee(String u, String p);
    public Employee getEmployeeByUsername(String username);
}
