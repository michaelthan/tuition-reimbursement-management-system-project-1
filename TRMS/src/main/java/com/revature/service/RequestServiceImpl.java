package com.revature.service;

import com.revature.models.Employee;
import com.revature.models.Request;
import com.revature.models.RequestStatuses;
import com.revature.repo.*;
import com.revature.util.HibernateUtil;
import com.revature.util.MyLogger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.query.Query;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class RequestServiceImpl implements RequestService{
    RequestRepo rr;
    public RequestServiceImpl (RequestRepo rr) {
        this.rr = rr;
    }
    @Override
    public Request getRequest(int id) {
        return rr.getRequest(id);
    }

    @Override
    public List<Request> getAllRequests() {
        return rr.getAllRequests();
    }

    @Override
    public Request addRequest(Request r) {
        return rr.addRequest(r);
    }

    @Override
    public Request updateRequest(Request change) {
        return rr.updateRequest(change);
    }

    @Override
    public Request deleteRequest(int id) {
        return rr.deleteRequest(id);
    }

    @Override
    public List<Request> getRequestsByUsername(String username) {
        EmployeeRepo er = new EmployeeRepoImpl();
        EmployeeService es = new EmployeeServiceImpl(er);
        Employee e = es.getEmployeeByUsername(username);
        List<Request> requestList = rr.getAllRequests();
        List<Request> userRequests = new ArrayList<Request>();
        for(Request r : requestList) {
            if(e.equals(r.getEmployee())){
                r.setEmployee(r.getEmployee());
                r.setTuitionType(r.getTuitionType());
                r.setStatus(r.getStatus());
                r.setGradingFormat(r.getGradingFormat());
                r.setApprovalEmail(r.getApprovalEmail());
                r.setPassingGradeDoc(r.getPassingGradeDoc());
                userRequests.add(r);
            }
        }
        if(userRequests.size() == 0)
            MyLogger.logger.error("request not found with username" + username);
        return userRequests;
    }

    @Override
    public List<Request> getRequestsToHandle(String username) {
        EmployeeRepo er = new EmployeeRepoImpl();
        EmployeeService es = new EmployeeServiceImpl(er);
        Employee e = es.getEmployeeByUsername(username);
        RequestStatusesRepo rsr = new RequestStatusesRepoImpl();
        RequestStatusesService rss = new RequestStatusServiceImpl(rsr);
        List<Request> requestList = rr.getAllRequests();
        List<Request> userRequests = new ArrayList<Request>();
        for (Request request : requestList) {
            //if user is CEO then approve it
            if (request.getEmployee().getSupervisor() == null && request.getStatus().getId() != 21) {
                request.setStatus(rss.getRequestStatus(21));
                rr.updateRequest(request);
            }
            //user is a department head and send request to benco, who is their supervisor
            if (request.getEmployee().getRole().getId() == 2 && request.getStatus().getId() == 2) {
                request.setStatus(rss.getRequestStatus(6));
                rr.updateRequest(request);
            }
            // user is benco and send request to ceo
            if (request.getEmployee().getRole().getId() == 3 && request.getStatus().getId() == 2) {
                request.setStatus(rss.getRequestStatus(8));
                rr.updateRequest(request);

            }
            // if the user is the supervisor and the request is in status of needing supervisor approval
            if (request.getStatus().getId() == 2 && request.getEmployee().getSupervisor().getUsername().equals(e.getUsername())) {
                request.setEmployee(request.getEmployee());
                request.setTuitionType(request.getTuitionType());
                request.setStatus(request.getStatus());
                request.setGradingFormat(request.getGradingFormat());
                request.setApprovalEmail(request.getApprovalEmail());
                request.setPassingGradeDoc(request.getPassingGradeDoc());
                userRequests.add(request);
            }
            // if the user is the department head and request is pending DH approval and user is in that department
            if (request.getStatus().getId() == 4 && request.getEmployee().getDepartment().getId() == e.getDepartment().getId() && e.getRole().getId() == 2) {
                request.setEmployee(request.getEmployee());
                request.setTuitionType(request.getTuitionType());
                request.setStatus(request.getStatus());
                request.setGradingFormat(request.getGradingFormat());
                request.setApprovalEmail(request.getApprovalEmail());
                request.setPassingGradeDoc(request.getPassingGradeDoc());
                userRequests.add(request);
            }
            // if the user is the benco and request is pending benco approval and user is in that department
            if ((request.getStatus().getId() == 6 && request.getEmployee().getDepartment().getId() == e.getDepartment().getId() && e.getRole().getId() == 3)||request.getStatus().getId() == 39
                    && request.getEmployee().getDepartment().getId() == e.getDepartment().getId() && e.getRole().getId() == 3) {
                request.setEmployee(request.getEmployee());
                request.setTuitionType(request.getTuitionType());
                request.setStatus(request.getStatus());
                request.setGradingFormat(request.getGradingFormat());
                request.setApprovalEmail(request.getApprovalEmail());
                request.setPassingGradeDoc(request.getPassingGradeDoc());
                userRequests.add(request);
            }
            // if the user is the ceo and the request is from the Benco pending CEO approval
            if (request.getStatus().getId() == 8 && request.getEmployee().getRole().getId() == 3 && e.getRole().getId() == 4) {
                request.setEmployee(request.getEmployee());
                request.setTuitionType(request.getTuitionType());
                request.setStatus(request.getStatus());
                request.setGradingFormat(request.getGradingFormat());
                request.setApprovalEmail(request.getApprovalEmail());
                request.setPassingGradeDoc(request.getPassingGradeDoc());
                userRequests.add(request);
            }


        }
        return userRequests;
    }
    @Override
    public Boolean requestAction(String action, int id) {
        RequestStatusesRepo rsr = new RequestStatusesRepoImpl();
        RequestStatusesService rss = new RequestStatusServiceImpl(rsr);
        if (action.equals("approve")) {
            Request r = rr.getRequest(id);
            if(r.getStatus().getId()==8) {
                r.setStatus(rss.getRequestStatus(22));
                rr.updateRequest(r);
                return true;
            }
            if(r.getStatus().getId() == 6){
                r.setStatus(rss.getRequestStatus(22));
                rr.updateRequest(r);
                return true;
            }
            if(r.getStatus().getId() == 2 || r.getStatus().getId() == 4){
                int status = r.getStatus().getId();
                status = status + 2;
                RequestStatuses rs = rss.getRequestStatus(status);
                r.setStatus(rss.getRequestStatus(status));
                rr.updateRequest(r);
                return true;
            }
            if(r.getStatus().getId() == 39) {
                r.setStatus(rss.getRequestStatus(38));
                rr.updateRequest(r);
                return true;
            }
        }
        if(action.equals("deny")) {
            Request r = rr.getRequest(id);
            if(r.getStatus().getId()==8) {
                r.setStatus(rss.getRequestStatus(14));
                rr.updateRequest(r);
                return true;
            }
            if(r.getStatus().getId() == 6 || r.getStatus().getId() ==22){
                r.setStatus(rss.getRequestStatus(12));
                rr.updateRequest(r);
                return true;
            }
            if(r.getStatus().getId() == 4){
                r.setStatus(rss.getRequestStatus(11));
                rr.updateRequest(r);
                return true;
            }
            if(r.getStatus().getId() == 2){
                r.setStatus(rss.getRequestStatus(10));
                rr.updateRequest(r);
                return true;
            }
            if(r.getStatus().getId() == 39){
                r.setStatus(rss.getRequestStatus(12));
                rr.updateRequest(r);
                return true;
            }
        }
        if(action.equals("requestedaction")) {
            Request r = rr.getRequest(id);
            if(r.getStatus().getId()==2){
                r.setStatus((rss.getRequestStatus(3)));
                rr.updateRequest(r);
                return true;
            }
            if(r.getStatus().getId()==4) {
                r.setStatus(rss.getRequestStatus(5));
                rr.updateRequest(r);
                return true;
            }
            if(r.getStatus().getId()==6) {
                r.setStatus(rss.getRequestStatus(7));
                rr.updateRequest(r);
                return true;
            }
            if(r.getStatus().getId()==8) {
                r.setStatus(rss.getRequestStatus(9));
                rr.updateRequest(r);
                return true;
            }
        }
        if(action.equals("gradedocument")) {
            Request r = rr.getRequest(id);
            r.setStatus(rss.getRequestStatus(39));
            rr.updateRequest(r);
            return true;
        }
        return false;
    }
}
