package com.revature.service;

import com.revature.models.Request;

import java.util.List;

public interface RequestService {
    public Request getRequest(int id);
    public List<Request> getAllRequests();
    public Request addRequest(Request r);
    public Request updateRequest(Request change);
    public Request deleteRequest(int id);

    public List<Request> getRequestsByUsername(String username);
    public List<Request> getRequestsToHandle(String username);
    public Boolean requestAction(String action, int id);
}
