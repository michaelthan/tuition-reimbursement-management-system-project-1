package com.revature.service;

import java.util.List;

public interface GradePresentationService {
    public com.revature.models.GradePresentation getGradePresentation(int id);
    public List<com.revature.models.GradePresentation> getAllGradePresentations();
    public com.revature.models.GradePresentation addGradePresentation(com.revature.models.GradePresentation g);
    public com.revature.models.GradePresentation updateGradePresentation(com.revature.models.GradePresentation change);
    public com.revature.models.GradePresentation deleteGradePresentation(int id);
}
