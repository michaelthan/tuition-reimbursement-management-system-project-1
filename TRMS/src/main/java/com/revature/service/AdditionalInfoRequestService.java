package com.revature.service;

import com.revature.models.AdditionalInfoRequest;

import java.util.List;

public interface AdditionalInfoRequestService {
    public AdditionalInfoRequest getAdditionalInfoRequest(int id);
    public List<AdditionalInfoRequest> getAllAdditionalInfoRequests();
    public AdditionalInfoRequest addAdditionalInfoRequest(AdditionalInfoRequest a);
    public AdditionalInfoRequest updateAdditionalInfoRequest(AdditionalInfoRequest change);
    public AdditionalInfoRequest deleteAdditionalInfoRequest(int id);
}
