package com.revature.service;

import com.revature.models.Roles;

import java.util.List;

public interface RolesService {
    public Roles getRoles(int id);
    public List<Roles> getAllRoles();
    public Roles addRole (Roles r);
    public Roles updateRole(Roles change);
    public Roles deleteRole(int id);
}
