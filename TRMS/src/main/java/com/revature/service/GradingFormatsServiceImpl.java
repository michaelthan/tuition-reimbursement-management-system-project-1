package com.revature.service;

import com.revature.models.GradingFormats;
import com.revature.repo.GradingFormatsRepo;

import java.util.List;

public class GradingFormatsServiceImpl implements GradingFormatsService{
    GradingFormatsRepo gr;
    public GradingFormatsServiceImpl (GradingFormatsRepo gr) {
        this.gr = gr;
    }
    @Override
    public GradingFormats getGradingFormat(int id) {
        return gr.getGradingFormat(id);
    }

    @Override
    public List<GradingFormats> getAllGradingFormats() {
        return gr.getAllGradingFormats();
    }

    @Override
    public GradingFormats addGradingFormat(GradingFormats g) {
        return gr.addGradingFormat(g);
    }

    @Override
    public GradingFormats updateGradingFormat(GradingFormats change) {
        return gr.updateGradingFormat(change);
    }

    @Override
    public GradingFormats deleteGradingFormat(int id) {
        return gr.deleteGradingFormat(id);
    }
}
