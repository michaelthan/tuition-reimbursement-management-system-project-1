package com.revature.service;

import com.revature.models.ApprovalEmail;
import com.revature.repo.ApprovalEmailRepo;

import java.util.List;

public class ApprovalEmailServiceImpl implements ApprovalEmailService{
    ApprovalEmailRepo ar;
    public ApprovalEmailServiceImpl (ApprovalEmailRepo ar) {
        this.ar = ar;
    }
    @Override
    public ApprovalEmail getApprovalEmail(int id) {
        return ar.getApprovalEmail(id);
    }

    @Override
    public List<ApprovalEmail> getAllApprovalEmails() {
        return ar.getAllApprovalEmails();
    }

    @Override
    public ApprovalEmail addApprovalEmail(ApprovalEmail a) {
        return ar.addApprovalEmail(a);
    }

    @Override
    public ApprovalEmail updateApprovalEmail(ApprovalEmail a) {
        return ar.updateApprovalEmail(a);
    }

    @Override
    public ApprovalEmail deleteApprovalEmail(int id) {
        return ar.deleteApprovalEmail(id);
    }
}
