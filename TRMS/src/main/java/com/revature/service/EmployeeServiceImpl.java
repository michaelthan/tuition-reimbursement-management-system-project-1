package com.revature.service;

import com.revature.models.Employee;
import com.revature.repo.EmployeeRepo;
import com.revature.util.HibernateUtil;
import com.revature.util.MyLogger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.query.Query;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class EmployeeServiceImpl implements EmployeeService {
    EmployeeRepo er;
    public EmployeeServiceImpl (EmployeeRepo er) {
        this.er = er;
    }
    @Override
    public Employee getEmployee(int id) {
        return er.getEmployee(id);
    }

    @Override
    public List<Employee> getAllEmployees() {
        return er.getAllEmployees();
    }

    @Override
    public Employee addEmployee(Employee e) {
        return er.addEmployee(e);
    }

    @Override
    public Employee updateEmployee(Employee e) {
        return er.updateEmployee(e);
    }

    @Override
    public Employee deleteEmployee(int id) {
        return er.deleteEmployee(id);
    }

    @Override
    public Boolean loginEmployee(String u, String p) {
        List<Employee> employeeList = er.getAllEmployees();
        for(Employee e: employeeList) {
            if(e.getUsername().equals(u)&&e.getPassword().equals(p)) {
                MyLogger.logger.info("Login success for " + u);
                return true;
            }
        }
        MyLogger.logger.warn("Invalid login credentials");
        return false;
    }
    public Employee getEmployeeByUsername(String username){
        Employee e = null;
        try(Session session = HibernateUtil.getSession()){
            CriteriaBuilder cb =session.getCriteriaBuilder();
            CriteriaQuery<Employee> cq = cb.createQuery(Employee.class);
            Root<Employee> root = cq.from(Employee.class);
            cq.select(root).where(cb.like(root.get("username"),username)).getOrderList();
            Query<Employee> q = session.createQuery(cq);
            e = q.getSingleResult();
            MyLogger.logger.info("get user by username");
            return e;
        }catch (HibernateException i) {
            i.printStackTrace();
        }
        return e;
    }
}
