package com.revature.service;

import com.revature.models.ReimbursementAmountAlteration;
import com.revature.repo.ReimbursementAmountAlterationRepo;

import java.util.List;

public class ReimbursementAmountAlterationServiceImpl implements ReimbursementAmountAlterationService{
    ReimbursementAmountAlterationRepo rr;
    public ReimbursementAmountAlterationServiceImpl (ReimbursementAmountAlterationRepo rr) {
        this.rr = rr;
    }
    @Override
    public ReimbursementAmountAlteration getReimbursementAmountAlteration(int id) {
        return rr.getReimbursementAmountAlteration(id);
    }

    @Override
    public List<ReimbursementAmountAlteration> getAllReimbursementAmountAlterations() {
        return rr.getAllReimbursementAmountAlterations();
    }

    @Override
    public ReimbursementAmountAlteration addReimbursementAmountAlteration(ReimbursementAmountAlteration r) {
        return rr.addReimbursementAmountAlteration(r);
    }

    @Override
    public ReimbursementAmountAlteration updateReimbursementAmountAlteration(ReimbursementAmountAlteration change) {
        return rr.updateReimbursementAmountAlteration(change);
    }

    @Override
    public ReimbursementAmountAlteration deleteReimbursementAmountAlteration(int id) {
        return rr.deleteReimbursementAmountAlteration(id);
    }
}
