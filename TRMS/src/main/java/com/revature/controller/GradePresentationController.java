package com.revature.controller;

import com.google.gson.Gson;
import com.revature.models.GradePresentation;
import com.revature.models.GradePresentation;
import com.revature.service.GradePresentationService;
import com.revature.util.MyLogger;
import io.javalin.http.Handler;

import java.util.List;

public class GradePresentationController {
    GradePresentationService gs;
    Gson gson = new Gson();
    public GradePresentationController (GradePresentationService gs){
        this.gs = gs;
    }
    public Handler getAllGradePresentation = (ctx) -> {
        List<GradePresentation> GradePresentationList = gs.getAllGradePresentations();
        ctx.result(gson.toJson(GradePresentationList));
    };
    public Handler getGradePresentationById = (ctx) ->{
        String input = ctx.pathParam("id");
        int id;
        try{
            id = Integer.parseInt(input);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            id=-1;
        }
        GradePresentation g = gs.getGradePresentation(id);
        if(g!=null) {
            ctx.result(gson.toJson(g));
        } else {
            ctx.status(404);
            MyLogger.logger.error("Could not get request with ID= " + id);
        }
    };
    public Handler addGradePresentation = (ctx) -> {
        GradePresentation g = gson.fromJson(ctx.body(),GradePresentation.class);
        g = gs.addGradePresentation(g);
        if(g!=null){
            ctx.status(201);
            ctx.result(gson.toJson(g));
        } else {
            MyLogger.logger.error("Could not create request with values: " + g);
            ctx.status(400);
        }
    };
    public Handler updateGradePresentation = (ctx) -> {
        String input = ctx.pathParam("id");
        int id;
        GradePresentation g = gson.fromJson(ctx.body(),GradePresentation.class);
        try{
            id=Integer.parseInt(input);
            g = gs.getGradePresentation(id);
        } catch (NumberFormatException i) {
            MyLogger.logger.error("invalid id");
            i.printStackTrace();
            id = -1;
        }
        g = gs.updateGradePresentation(g);
        if(g!=null) {
            ctx.result(gson.toJson(g));
        } else {
            ctx.status(404);
            MyLogger.logger.error("no client with id= " + id);
        }
    };
    public Handler deleteGradePresentation = (ctx) -> {
        String input = ctx.pathParam("id");
        int id;
        GradePresentation g = new GradePresentation();
        try{
            id = Integer.parseInt(input);
            g = gs.getGradePresentation(id);
        } catch (NumberFormatException i) {
            i.printStackTrace();
            MyLogger.logger.error("Invalid id");
        }
        if(g!=null) {
            ctx.result(gson.toJson(g));
            ctx.status(204);
        } else {
            ctx.status(404);
        }
    };
}
