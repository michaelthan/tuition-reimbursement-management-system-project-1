package com.revature.controller;

import com.google.gson.Gson;
import com.revature.models.Employee;
import com.revature.service.EmployeeService;
import com.revature.util.MyLogger;
import io.javalin.http.Handler;

import java.util.List;


public class EmployeeController {
    EmployeeService es;
    Gson gson = new Gson();
    public EmployeeController(EmployeeService es) {
        this.es = es;
    }
    public Handler getAllEmployee = (ctx) -> {
        List<Employee> employeeList = es.getAllEmployees();
        ctx.result(gson.toJson(employeeList));
    };
    public Handler getEmployeeByUsername = (ctx) -> {
        String username = ctx.pathParam("username");
        Employee e = es.getEmployeeByUsername(username);
        if(e!=null) {
            ctx.result(gson.toJson(e));
        } else {
            ctx.status(404);
            MyLogger.logger.error("Could not get request with username= " + username);
        }
    };
    public Handler getEmployeeById = (ctx) ->{
        String input = ctx.pathParam("id");
        int id;
        try{
            id = Integer.parseInt(input);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            id=-1;
        }
        Employee e = es.getEmployee(id);
        if(e!=null) {
            ctx.result(gson.toJson(e));
        } else {
            ctx.status(404);
            MyLogger.logger.error("Could not get request with ID= " + id);
        }
    };
    public Handler addEmployee = (ctx) -> {
        Employee e = gson.fromJson(ctx.body(),Employee.class);
        e = es.addEmployee(e);
        if(e!=null){
            ctx.status(201);
            ctx.result(gson.toJson(e));
        } else {
            MyLogger.logger.error("Could not create request with values: " + e);
            ctx.status(400);
        }
    };
    public Handler updateEmployee = (ctx) -> {
        String input = ctx.pathParam("id");
        int id;
        Employee e = gson.fromJson(ctx.body(),Employee.class);
        try{
            id=Integer.parseInt(input);
            e = es.getEmployee(id);
        } catch (NumberFormatException i) {
            MyLogger.logger.error("invalid id");
            i.printStackTrace();
            id = -1;
        }
        e = es.updateEmployee(e);
        if(e!=null) {
            ctx.result(gson.toJson(e));
        } else {
            ctx.status(404);
            MyLogger.logger.error("no client with id= " + id);
        }
    };
    public Handler deleteEmployee = (ctx) -> {
        String input = ctx.pathParam("id");
        int id;
        Employee e = new Employee();
        try{
            id = Integer.parseInt(input);
            e = es.getEmployee(id);
        } catch (NumberFormatException i) {
            i.printStackTrace();
            MyLogger.logger.error("Invalid id");
        }
        if(e!=null) {
            ctx.result(gson.toJson(e));
            ctx.status(204);
        } else {
            ctx.status(404);
        }
    };
    public Handler loginEmployee = (ctx) -> {
        String username = ctx.headerMap().get("username");
        String password = ctx.headerMap().get("password");
        Boolean login = es.loginEmployee(username,password);
        if(!login) {
            MyLogger.logger.warn("Invalid login");
            ctx.status(400);
            ctx.result("false");
        } else {
            MyLogger.logger.info("login success for " + username);
            ctx.result("true");
        }
    };


}
