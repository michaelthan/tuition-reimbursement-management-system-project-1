package com.revature.controller;

import com.google.gson.Gson;
import com.revature.models.RequestStatuses;
import com.revature.models.TuitionTypes;
import com.revature.service.TuitionTypesService;
import com.revature.util.HibernateUtil;
import com.revature.util.MyLogger;
import io.javalin.http.Handler;
import org.hibernate.HibernateException;
import org.hibernate.Session;

public class TuitionTypesController {
    TuitionTypesService ts;
    Gson gson = new Gson();
    
    public TuitionTypesController(TuitionTypesService ts) {
        this.ts = ts;
    }
    public Handler getTuitionTypes = (ctx) -> {
        String input = ctx.pathParam("id");
        int id;
        try{
            id = Integer.parseInt(input);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            id=-1;
        }
        TuitionTypes r = ts.getTuitionTypes(id);
        if(r!=null) {
            ctx.result(gson.toJson(r));
        } else {
            ctx.status(404);
            MyLogger.logger.error("Could not get tuition type with ID= " + id);
        }
    };
}
