package com.revature.controller;


import com.google.gson.Gson;
import com.revature.models.ApprovalEmail;
import com.revature.service.ApprovalEmailService;
import com.revature.util.MyLogger;
import io.javalin.http.Handler;

import java.util.List;

public class ApprovalEmailController {
    ApprovalEmailService as;
    Gson gson = new Gson();
    public ApprovalEmailController(ApprovalEmailService as) {
        this.as =as;
    }
    public Handler getAllApprovalEmail = (ctx) -> {
        List<ApprovalEmail> approvalEmailControllerList = as.getAllApprovalEmails();
        ctx.result(gson.toJson(approvalEmailControllerList));
    };
    public Handler getApprovalEmailById = (ctx) ->{
        String input = ctx.pathParam("id");
        int id;
        try{
            id = Integer.parseInt(input);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            id=-1;
        }
        ApprovalEmail a = as.getApprovalEmail(id);
        if(a!=null) {
            ctx.result(gson.toJson(a));
        } else {
            ctx.status(404);
            MyLogger.logger.error("Could not get request with ID= " + id);
        }
    };
    public Handler addApprovalEmail = (ctx) -> {
        ApprovalEmail a = gson.fromJson(ctx.body(), ApprovalEmail.class);
        a = as.addApprovalEmail(a);
        if(a!=null){
            ctx.status(201);
            ctx.result(gson.toJson(a));
        } else {
            MyLogger.logger.error("Could not create request with values: " + a);
            ctx.status(400);
        }
    };
    public Handler updateApprovalEmail = (ctx) -> {
        String input = ctx.pathParam("id");
        int id;
        ApprovalEmail a = gson.fromJson(ctx.body(), ApprovalEmail.class);
        try{
            id=Integer.parseInt(input);
            a = as.getApprovalEmail(id);
        } catch (NumberFormatException i) {
            MyLogger.logger.error("invalid id");
            i.printStackTrace();
            id = -1;
        }
        a = as.updateApprovalEmail(a);
        if(a!=null) {
            ctx.result(gson.toJson(a));
        } else {
            ctx.status(404);
            MyLogger.logger.error("no client with id= " + id);
        }
    };
    public Handler deleteApprovalEmail = (ctx) -> {
        String input = ctx.pathParam("id");
        int id;
        ApprovalEmail a = new ApprovalEmail();
        try{
            id = Integer.parseInt(input);
            a = as.getApprovalEmail(id);
        } catch (NumberFormatException i) {
            i.printStackTrace();
            MyLogger.logger.error("Invalid id");
        }
        if(a!=null) {
            ctx.result(gson.toJson(a));
            ctx.status(204);
        } else {
            ctx.status(404);
        }
    };
}
