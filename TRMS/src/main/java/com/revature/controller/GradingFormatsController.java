package com.revature.controller;

import com.google.gson.Gson;
import com.revature.models.GradingFormats;
import com.revature.service.GradingFormatsService;
import com.revature.util.MyLogger;
import io.javalin.http.Handler;

public class GradingFormatsController {
    GradingFormatsService gs;
    Gson gson = new Gson();
    public GradingFormatsController (GradingFormatsService gs) {
        this.gs = gs;
    }
    public Handler getGradingFormatsById = (ctx) ->{
        String input = ctx.pathParam("id");
        int id;
        try{
            id = Integer.parseInt(input);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            id=-1;
        }
        GradingFormats g = gs.getGradingFormat(id);
        if(g!=null) {
            ctx.result(gson.toJson(g));
        } else {
            ctx.status(404);
            MyLogger.logger.error("Could not get request with ID= " + id);
        }
    };
}
