package com.revature.controller;

import com.google.gson.Gson;
import com.revature.models.Request;
import com.revature.models.RequestStatuses;
import com.revature.service.RequestStatusesService;
import com.revature.util.MyLogger;
import io.javalin.http.Handler;

public class RequestStatusController {
    RequestStatusesService rs;
    Gson gson = new Gson();

    public RequestStatusController (RequestStatusesService rs) {
        this.rs = rs;
    }
    public Handler getRequestStatusById = (ctx) -> {
        String input = ctx.pathParam("id");
        int id;
        try{
            id = Integer.parseInt(input);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            id=-1;
        }
        RequestStatuses r = rs.getRequestStatus(id);
        System.out.println(id);
        System.out.println(r);
        if(r!=null) {
            ctx.result(gson.toJson(r));
        } else {
            ctx.status(404);
            MyLogger.logger.error("Could not get requeststatus with ID= " + id);
        }
    };
}
