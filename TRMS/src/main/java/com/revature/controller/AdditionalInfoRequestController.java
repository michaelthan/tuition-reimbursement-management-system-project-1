package com.revature.controller;

import com.google.gson.Gson;
import com.revature.models.AdditionalInfoRequest;
import com.revature.service.AdditionalInfoRequestService;
import com.revature.util.MyLogger;
import io.javalin.http.Handler;

import java.util.List;

public class AdditionalInfoRequestController {
    AdditionalInfoRequestService as;
    Gson gson = new Gson();
    public AdditionalInfoRequestController(AdditionalInfoRequestService as) {
        this.as =as;
    }
    public Handler getAllAdditionalInfoRequest = (ctx) -> {
        List<AdditionalInfoRequest> AdditionalInfoRequestList = as.getAllAdditionalInfoRequests();
        ctx.result(gson.toJson(AdditionalInfoRequestList));
    };
    public Handler getAdditionalInfoRequestById = (ctx) ->{
        String input = ctx.pathParam("id");
        int id;
        try{
            id = Integer.parseInt(input);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            id=-1;
        }
        AdditionalInfoRequest a = as.getAdditionalInfoRequest(id);
        if(a!=null) {
            ctx.result(gson.toJson(a));
        } else {
            ctx.status(404);
            MyLogger.logger.error("Could not get request with ID= " + id);
        }
    };
    public Handler addAdditionalInfoRequest = (ctx) -> {
        AdditionalInfoRequest a = gson.fromJson(ctx.body(),AdditionalInfoRequest.class);
        a = as.addAdditionalInfoRequest(a);
        if(a!=null){
            ctx.status(201);
            ctx.result(gson.toJson(a));
        } else {
            MyLogger.logger.error("Could not create request with values: " + a);
            ctx.status(400);
        }
    };
    public Handler updateAdditionalInfoRequest = (ctx) -> {
        String input = ctx.pathParam("id");
        int id;
        AdditionalInfoRequest a = gson.fromJson(ctx.body(),AdditionalInfoRequest.class);
        try{
            id=Integer.parseInt(input);
            a = as.getAdditionalInfoRequest(id);
        } catch (NumberFormatException i) {
            MyLogger.logger.error("invalid id");
            i.printStackTrace();
            id = -1;
        }
        a = as.updateAdditionalInfoRequest(a);
        if(a!=null) {
            ctx.result(gson.toJson(a));
        } else {
            ctx.status(404);
            MyLogger.logger.error("no client with id= " + id);
        }
    };
    public Handler deleteAdditionalInfoRequest = (ctx) -> {
        String input = ctx.pathParam("id");
        int id;
        AdditionalInfoRequest a = new AdditionalInfoRequest();
        try{
            id = Integer.parseInt(input);
            a = as.getAdditionalInfoRequest(id);
        } catch (NumberFormatException i) {
            i.printStackTrace();
            MyLogger.logger.error("Invalid id");
        }
        if(a!=null) {
            ctx.result(gson.toJson(a));
            ctx.status(204);
        } else {
            ctx.status(404);
        }
    };
}
