package com.revature.controller;

import com.google.gson.Gson;
import com.revature.models.ReimbursementAmountAlteration;
import com.revature.service.ReimbursementAmountAlterationService;
import com.revature.util.MyLogger;
import io.javalin.http.Handler;

import java.util.List;

public class ReimbursementAmountAlterationController {
    ReimbursementAmountAlterationService rs;
    Gson gson = new Gson();
    public ReimbursementAmountAlterationController (ReimbursementAmountAlterationService rs) {
        this.rs = rs;
    }
    public Handler getAllReimbursementAmountAlteration = (ctx) -> {
        List<ReimbursementAmountAlteration> ReimbursementAmountAlterationList = rs.getAllReimbursementAmountAlterations();
        ctx.result(gson.toJson(ReimbursementAmountAlterationList));
    };
    public Handler getReimbursementAmountAlterationById = (ctx) ->{
        String input = ctx.pathParam("id");
        int id;
        try{
            id = Integer.parseInt(input);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            id=-1;
        }
        ReimbursementAmountAlteration r = rs.getReimbursementAmountAlteration(id);
        if(r!=null) {
            ctx.result(gson.toJson(r));
        } else {
            ctx.status(404);
            MyLogger.logger.error("Could not get request with ID= " + id);
        }
    };
    public Handler addReimbursementAmountAlteration = (ctx) -> {
        ReimbursementAmountAlteration r = gson.fromJson(ctx.body(),ReimbursementAmountAlteration.class);
        r = rs.addReimbursementAmountAlteration(r);
        if(r!=null){
            ctx.status(201);
            ctx.result(gson.toJson(r));
        } else {
            MyLogger.logger.error("Could not create request with values: " + r);
            ctx.status(400);
        }
    };
    public Handler updateReimbursementAmountAlteration = (ctx) -> {
        String input = ctx.pathParam("id");
        int id;
        ReimbursementAmountAlteration r = gson.fromJson(ctx.body(),ReimbursementAmountAlteration.class);
        try{
            id=Integer.parseInt(input);
            r = rs.getReimbursementAmountAlteration(id);
        } catch (NumberFormatException i) {
            MyLogger.logger.error("invalid id");
            i.printStackTrace();
            id = -1;
        }
        r = rs.updateReimbursementAmountAlteration(r);
        if(r!=null) {
            ctx.result(gson.toJson(r));
        } else {
            ctx.status(404);
            MyLogger.logger.error("no client with id= " + id);
        }
    };
    public Handler deleteReimbursementAmountAlteration = (ctx) -> {
        String input = ctx.pathParam("id");
        int id;
        ReimbursementAmountAlteration r = new ReimbursementAmountAlteration();
        try{
            id = Integer.parseInt(input);
            r = rs.getReimbursementAmountAlteration(id);
        } catch (NumberFormatException i) {
            i.printStackTrace();
            MyLogger.logger.error("Invalid id");
        }
        if(r!=null) {
            ctx.result(gson.toJson(r));
            ctx.status(204);
        } else {
            ctx.status(404);
        }
    };
}
