package com.revature.controller;

import com.google.gson.Gson;
import com.revature.models.Request;
import com.revature.service.RequestService;
import com.revature.util.MyLogger;
import io.javalin.http.Handler;

import java.util.List;

public class RequestController {
    RequestService rs;
    Gson gson = new Gson();

    public RequestController(RequestService rs){
        this.rs = rs;
    }

    public Handler getAllRequest = (ctx) -> {
        List<Request> requestList = rs.getAllRequests();
        ctx.result(gson.toJson(requestList));
    };
    public Handler getRequestById = (ctx) -> {
        String input = ctx.pathParam("id");
        int id;
        try{
            id = Integer.parseInt(input);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            id=-1;
        }
        Request r = rs.getRequest(id);
        if(r!=null) {
            ctx.result(gson.toJson(r));
        } else {
            ctx.status(404);
            MyLogger.logger.error("Could not get request with ID= " + id);
        }
    };
    public Handler addRequest = (ctx) -> {
        Request r = gson.fromJson(ctx.body(),Request.class);
        r = rs.addRequest(r);
        System.out.println("here");
        if(r!=null){
            MyLogger.logger.info("created request");
            ctx.status(201);
            ctx.result(gson.toJson(r));
        } else {
            MyLogger.logger.error("Could not create request with values: " + r);
            ctx.status(400);
        }
    };
    public Handler updateRequest = (ctx) ->{
        String input = ctx.pathParam("id");
        int id;
        Request r = gson.fromJson(ctx.body(),Request.class);
        try{
            id=Integer.parseInt(input);
            r = rs.getRequest(id);
        } catch (NumberFormatException e) {
            MyLogger.logger.error("invalid id");
            e.printStackTrace();
            id = -1;
        }
        r = rs.updateRequest(r);
        if(r!=null) {
            ctx.result(gson.toJson(r));
        } else {
            ctx.status(404);
            MyLogger.logger.error("no user with id= " + id);
        }
    };
    public Handler deleteRequest = (ctx) -> {
        String input = ctx.pathParam("id");
        int id;
        Request r = new Request();
        try{
            id = Integer.parseInt(input);
            r = rs.getRequest(id);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            MyLogger.logger.error("Invalid id");
        }
        if(r!=null) {
            ctx.result(gson.toJson(r));
            ctx.status(204);
        } else {
            ctx.status(404);
        }
    };
    public Handler getRequestByUsername = (ctx) -> {
        String username = ctx.pathParam("username");
        List<Request> r = rs.getRequestsByUsername(username);
        if(r!=null) {
            ctx.result(gson.toJson(r));
        } else {
            ctx.status(404);
            MyLogger.logger.error("Could not get requests with username = " + username);
        }
    };
    public Handler getRequestsToHandle = (ctx) -> {
        String username = ctx.pathParam("username");
        List<Request> r = rs.getRequestsToHandle(username);
        if(r!=null) {
            ctx.result(gson.toJson(r));
        } else {
            ctx.status(404);
            MyLogger.logger.error("Could not get requests for username = " + username);
        }
    };
    public Handler requestAction = (ctx) -> {
        String action = ctx.pathParam("action");
        String input = ctx.pathParam("id");
        int id;
        Request r = null;
        try{
            id = Integer.parseInt(input);
            r = rs.getRequest(id);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            MyLogger.logger.error("Invalid request id");
        }
        Boolean result = rs.requestAction(action,r.getId());
        ctx.result(gson.toJson(result));
    };

}
