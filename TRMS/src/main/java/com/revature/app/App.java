package com.revature.app;

import com.google.gson.Gson;
import com.revature.controller.*;
import com.revature.models.*;
import com.revature.repo.*;
import com.revature.service.*;
import io.javalin.Javalin;

import java.util.List;

public class App {
    public static void main(String[] args) {
        EmployeeRepo er = new EmployeeRepoImpl();
        EmployeeService es = new EmployeeServiceImpl(er);
        EmployeeController ec = new EmployeeController(es);
        RolesRepo rr = new RolesRepoImpl();
        RolesService rs = new RolesServiceImpl(rr);
        Gson gson = new Gson();

        Javalin app = Javalin.create(config -> {
            config.enableCorsForAllOrigins();
//            config.accessManager((handler, ctx, permittedRoles)->{
//                if(ctx.basicAuthCredentialsExist()){
//                    String username = ctx.basicAuthCredentials().getUsername();
//                    String password = ctx.basicAuthCredentials().getPassword();
//                    List<Employee> employeeList = es.getAllEmployees();
//                    for(Employee e : employeeList) {
//                        if(e.getUsername().equals(username) && e.getPassword().equals(password)){
//                            ctx.result(gson.toJson(e.getUsername()));
//                        }
//                    }
//                } else {
//                    ctx.status(401).result("Unauthorized");
//                }
//            });
//
        });
        establishRoutes(app);
        app.start(7001);
    }

    private static void establishRoutes(Javalin app) {
        //login
        EmployeeRepo er = new EmployeeRepoImpl();
        EmployeeService es = new EmployeeServiceImpl(er);
        EmployeeController ec = new EmployeeController(es);
        app.get("/login", ec.loginEmployee);
        app.get("/employee/:username", ec.getEmployeeByUsername);
        app.get("/employeeid/:id", ec.getEmployeeById);

        RequestRepo rr = new RequestRepoImpl();
        RequestService rs = new RequestServiceImpl(rr);
        RequestController rc = new RequestController(rs);
        app.post("/request", rc.addRequest);
        app.put("/request/:id",rc.updateRequest);
//        app.get("/request/:id", rc.getRequestById);
        app.get("/request/:username", rc.getRequestByUsername);
        app.get("/request/action/:username", rc.getRequestsToHandle);
        app.get("/request/:action/:id", rc.requestAction);

        GradingFormatsRepo gr = new GradingFormatsRepoImpl();
        GradingFormatsService gs = new GradingFormatsServiceImpl(gr);
        GradingFormatsController gc = new GradingFormatsController(gs);
        app.get("/gradingformat/:id", gc.getGradingFormatsById);

        RequestStatusesRepo rsr = new RequestStatusesRepoImpl();
        RequestStatusesService rss = new RequestStatusServiceImpl(rsr);
        RequestStatusController rsc = new RequestStatusController(rss);
        app.get("/requeststatus/:id",rsc.getRequestStatusById);

        TuitionTypesRepo ttr = new TuitionTypesRepoImpl();
        TuitionTypesService tts = new TuitionTypesServiceImpl(ttr);
        TuitionTypesController ttc = new TuitionTypesController(tts);
        app.get("/tuitiontype/:id", ttc.getTuitionTypes);


    }
}
