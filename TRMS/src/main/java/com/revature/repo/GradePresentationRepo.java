package com.revature.repo;

import com.revature.models.GradePresentation;

import java.util.List;

public interface GradePresentationRepo {
    public GradePresentation getGradePresentation(int id);
    public List<GradePresentation> getAllGradePresentations();
    public GradePresentation addGradePresentation(GradePresentation g);
    public GradePresentation updateGradePresentation(GradePresentation change);
    public GradePresentation deleteGradePresentation(int id);
}
