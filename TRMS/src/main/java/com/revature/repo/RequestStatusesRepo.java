package com.revature.repo;

import com.revature.models.RequestStatuses;

import java.util.List;

public interface RequestStatusesRepo {
    public RequestStatuses getRequestStatus (int id);
    public List<RequestStatuses> getAllRequestStatuses();
    public RequestStatuses addRequestStatus(RequestStatuses r);
    public RequestStatuses updateRequestStatus(RequestStatuses change);
    public RequestStatuses deleteRequestStatus(int id);
}
