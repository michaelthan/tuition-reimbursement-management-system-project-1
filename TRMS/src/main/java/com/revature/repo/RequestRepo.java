package com.revature.repo;

import com.revature.models.Request;

import java.util.List;

public interface RequestRepo {
    public Request getRequest(int id);
    public List<Request> getAllRequests();
    public Request addRequest(Request r);
    public Request updateRequest(Request change);
    public Request deleteRequest(int id);
}
