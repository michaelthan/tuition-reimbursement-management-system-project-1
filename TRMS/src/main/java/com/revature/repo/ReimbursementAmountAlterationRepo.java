package com.revature.repo;

import com.revature.models.ReimbursementAmountAlteration;

import java.util.List;

public interface ReimbursementAmountAlterationRepo {
    public ReimbursementAmountAlteration getReimbursementAmountAlteration (int id);
    public List<ReimbursementAmountAlteration> getAllReimbursementAmountAlterations();
    public ReimbursementAmountAlteration addReimbursementAmountAlteration(ReimbursementAmountAlteration r);
    public ReimbursementAmountAlteration updateReimbursementAmountAlteration(ReimbursementAmountAlteration change);
    public ReimbursementAmountAlteration deleteReimbursementAmountAlteration(int id);
}
