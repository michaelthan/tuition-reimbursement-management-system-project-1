package com.revature.repo;

import com.revature.models.Roles;
import com.revature.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class RolesRepoImpl implements RolesRepo{

    @Override
    public Roles getRoles(int id) {
        Session session = HibernateUtil.getSession();
        Roles r;
        try{
            r = session.get(Roles.class,id);
            return r;
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return null;
    }

    @Override
    public List<Roles> getAllRoles() {
        Session session = HibernateUtil.getSession();
        List<Roles> rolesList = null;
        try {
            rolesList = session.createQuery("FROM Roles").list();
        }catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return rolesList;
    }

    @Override
    public Roles addRole(Roles r) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            r.setId((int)session.save(r));
            tx.commit();
        }catch (HibernateException e){
            e.printStackTrace();
            if(tx!=null) tx.rollback();
            return null;
        } finally {
            session.close();
        }
        return r;
    }

    @Override
    public Roles updateRole(Roles change) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            session.update(change);
            tx.commit();
        }catch (HibernateException e){
            e.printStackTrace();
            if(tx!=null) tx.rollback();
            return null;
        }finally {
            session.close();
        }
        return change;
    }

    @Override
    public Roles deleteRole(int id) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        Roles r;
        try{
            tx = session.beginTransaction();
            r = session.get(Roles.class, id);
            session.delete(r);
            tx.commit();
        }catch (HibernateException e){
            e.printStackTrace();
            if(tx!=null) tx.rollback();
            return null;
        }finally {
            session.close();
        }
        return r;
    }
}
