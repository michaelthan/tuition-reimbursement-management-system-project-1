package com.revature.repo;

import com.revature.models.Departments;
import com.revature.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.io.FileInputStream;
import java.util.List;

public class DepartmentsRepoImpl implements DepartmentsRepo{
    @Override
    public Departments getDepartment(int id) {
        Session session = HibernateUtil.getSession();
        Departments d = null;
        try{
            d = session.get(Departments.class,id);
        }catch (HibernateException e){
            e.printStackTrace();
        } finally {
            session.close();
        }
        return d;
    }

    @Override
    public List<Departments> getAllDepartments() {
        Session session = HibernateUtil.getSession();
        List<Departments> departmentsList = null;
        try{
            departmentsList = session.createQuery("FROM Departments").list();
        }catch (HibernateException e){
            e.printStackTrace();
        } finally {
            session.close();
        }
        return departmentsList;
    }

    @Override
    public Departments addDepartment(Departments d) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            d.setId((int)session.save(d));
            tx.commit();
        }catch (HibernateException e){
            e.printStackTrace();
            if(tx!=null) tx.rollback();
            return null;
        }finally {
            session.close();
        }
        return d;
    }

    @Override
    public Departments updateDepartment(Departments d) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            session.update(d);
            tx.commit();
        } catch (HibernateException e){
            e.printStackTrace();
            if(tx!=null) tx.rollback();
            return null;
        } finally {
            session.close();
        }
        return d;
    }

    @Override
    public Departments deleteDepartment(int id) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        Departments d= null;
        try{
            tx = session.beginTransaction();
            d = session.get(Departments.class,id);
            session.delete(d);
            tx.commit();
        }catch (HibernateException e) {
            e.printStackTrace();
            if(tx!=null) tx.rollback();
            d = null;
        }finally {
            session.close();
        }
        return d;
    }
}
