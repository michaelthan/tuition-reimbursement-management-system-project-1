package com.revature.repo;

import com.revature.models.ApprovalEmail;
import com.revature.models.Employee;
import com.revature.models.GradingFormats;
import com.revature.util.HibernateUtil;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.proxy.HibernateProxy;

import java.util.List;

public class ApprovalEmailRepoImpl implements ApprovalEmailRepo{
    @Override
    public ApprovalEmail getApprovalEmail(int id) {
        Session session = HibernateUtil.getSession();
        ApprovalEmail r;
        try{
            r = session.get(ApprovalEmail.class,id);
            r.setApprovedBy(r.getApprovedBy());
            return r;
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return null;
    }

    @Override
    public List<ApprovalEmail> getAllApprovalEmails() {
        Session session = HibernateUtil.getSession();
        List<ApprovalEmail> ApprovalEmailList = null;
        try {
            ApprovalEmailList = session.createQuery("FROM ApprovalEmail").list();
        }catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return ApprovalEmailList;
    }

    @Override
    public ApprovalEmail addApprovalEmail(ApprovalEmail r) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            r.setId((int)session.save(r));
            tx.commit();
        }catch (HibernateException e){
            e.printStackTrace();
            if(tx!=null) tx.rollback();
            return null;
        } finally {
            session.close();
        }
        return r;
    }

    @Override
    public ApprovalEmail updateApprovalEmail(ApprovalEmail change) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            session.update(change);
            tx.commit();
        }catch (HibernateException e){
            e.printStackTrace();
            if(tx!=null) tx.rollback();
            return null;
        }finally {
            session.close();
        }
        return change;
    }

    @Override
    public ApprovalEmail deleteApprovalEmail(int id) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        ApprovalEmail r;
        try{
            tx = session.beginTransaction();
            r = session.get(ApprovalEmail.class, id);
            session.delete(r);
            tx.commit();
        }catch (HibernateException e){
            e.printStackTrace();
            if(tx!=null) tx.rollback();
            return null;
        }finally {
            session.close();
        }
        return r;
    }
}
