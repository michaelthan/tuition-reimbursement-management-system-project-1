package com.revature.repo;

import com.revature.models.RequestStatuses;
import com.revature.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class RequestStatusesRepoImpl implements RequestStatusesRepo{
    @Override
    public RequestStatuses getRequestStatus(int id) {
        Session session = HibernateUtil.getSession();
        RequestStatuses r;
        try{
            r = session.get(RequestStatuses.class,id);
            return r;
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return null;
    }

    @Override
    public List<RequestStatuses> getAllRequestStatuses() {
        Session session = HibernateUtil.getSession();
        List<RequestStatuses> RequestStatusesList = null;
        try {
            RequestStatusesList = session.createQuery("FROM RequestStatuses").list();
        }catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return RequestStatusesList;
    }

    @Override
    public RequestStatuses addRequestStatus(RequestStatuses r) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            r.setId((int)session.save(r));
            tx.commit();
        }catch (HibernateException e){
            e.printStackTrace();
            if(tx!=null) tx.rollback();
            return null;
        } finally {
            session.close();
        }
        return r;
    }

    @Override
    public RequestStatuses updateRequestStatus(RequestStatuses change) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            session.update(change);
            tx.commit();
        }catch (HibernateException e){
            e.printStackTrace();
            if(tx!=null) tx.rollback();
            return null;
        }finally {
            session.close();
        }
        return change;
    }

    @Override
    public RequestStatuses deleteRequestStatus(int id) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        RequestStatuses r;
        try{
            tx = session.beginTransaction();
            r = session.get(RequestStatuses.class, id);
            session.delete(r);
            tx.commit();
        }catch (HibernateException e){
            e.printStackTrace();
            if(tx!=null) tx.rollback();
            return null;
        }finally {
            session.close();
        }
        return r;
    }
}
