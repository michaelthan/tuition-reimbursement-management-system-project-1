package com.revature.repo;

import com.revature.models.GradePresentation;
import com.revature.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class GradePresentationRepoImpl implements GradePresentationRepo{
    @Override
    public GradePresentation getGradePresentation(int id) {
        Session session = HibernateUtil.getSession();
        GradePresentation r;
        try{
            r = session.get(GradePresentation.class,id);
            return r;
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return null;
    }

    @Override
    public List<GradePresentation> getAllGradePresentations() {
        Session session = HibernateUtil.getSession();
        List<GradePresentation> GradePresentationList = null;
        try {
            GradePresentationList = session.createQuery("FROM GradePresentation").list();
        }catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return GradePresentationList;
    }

    @Override
    public GradePresentation addGradePresentation(GradePresentation r) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            r.setId((int)session.save(r));
            tx.commit();
        }catch (HibernateException e){
            e.printStackTrace();
            if(tx!=null) tx.rollback();
            return null;
        } finally {
            session.close();
        }
        return r;
    }

    @Override
    public GradePresentation updateGradePresentation(GradePresentation change) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            session.update(change);
            tx.commit();
        }catch (HibernateException e){
            e.printStackTrace();
            if(tx!=null) tx.rollback();
            return null;
        }finally {
            session.close();
        }
        return change;
    }

    @Override
    public GradePresentation deleteGradePresentation(int id) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        GradePresentation r;
        try{
            tx = session.beginTransaction();
            r = session.get(GradePresentation.class, id);
            session.delete(r);
            tx.commit();
        }catch (HibernateException e){
            e.printStackTrace();
            if(tx!=null) tx.rollback();
            return null;
        }finally {
            session.close();
        }
        return r;
    }
}
