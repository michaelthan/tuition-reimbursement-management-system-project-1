package com.revature.repo;

import com.revature.models.Employee;
import com.revature.util.HibernateUtil;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.proxy.HibernateProxy;

import java.util.List;

public class EmployeeRepoImpl implements EmployeeRepo{
    @Override
    public Employee getEmployee(int id) {
        Session session = HibernateUtil.getSession();
        Employee r;
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            r = session.get(Employee.class,id);
            tx.commit();
            return r;
        } catch (HibernateException e) {
            e.printStackTrace();
            if(tx!=null) tx.rollback();
        } finally {
            session.close();
        }
        return null;
    }

    @Override
    public List<Employee> getAllEmployees() {
        Session session = HibernateUtil.getSession();
        List<Employee> EmployeeList = null;
        try {
            EmployeeList = session.createQuery("FROM Employee").list();
        }catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return EmployeeList;
    }

    @Override
    public Employee addEmployee(Employee r) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            r.setId((int)session.save(r));
            tx.commit();
        }catch (HibernateException e){
            e.printStackTrace();
            if(tx!=null) tx.rollback();
            return null;
        } finally {
            session.close();
        }
        return r;
    }

    @Override
    public Employee updateEmployee(Employee change) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            session.update(change);
            tx.commit();
        }catch (HibernateException e){
            e.printStackTrace();
            if(tx!=null) tx.rollback();
            return null;
        }finally {
            session.close();
        }
        return change;
    }

    @Override
    public Employee deleteEmployee(int id) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        Employee r;
        try{
            tx = session.beginTransaction();
            r = session.get(Employee.class, id);
            session.delete(r);
            tx.commit();
        }catch (HibernateException e){
            e.printStackTrace();
            if(tx!=null) tx.rollback();
            return null;
        }finally {
            session.close();
        }
        return r;
    }
}
