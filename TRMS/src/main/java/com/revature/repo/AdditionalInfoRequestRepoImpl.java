package com.revature.repo;

import com.revature.models.AdditionalInfoRequest;
import com.revature.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class AdditionalInfoRequestRepoImpl implements AdditionalInfoRequestRepo{
    @Override
    public AdditionalInfoRequest getAdditionalInfoRequest(int id) {
        Session session = HibernateUtil.getSession();
        AdditionalInfoRequest r;
        try{
            r = session.get(AdditionalInfoRequest.class,id);
            return r;
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return null;
    }

    @Override
    public List<AdditionalInfoRequest> getAllAdditionalInfoRequests() {
        Session session = HibernateUtil.getSession();
        List<AdditionalInfoRequest> AdditionalInfoRequestList = null;
        try {
            AdditionalInfoRequestList = session.createQuery("FROM AdditionalInfoRequest").list();
        }catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return AdditionalInfoRequestList;
    }

    @Override
    public AdditionalInfoRequest addAdditionalInfoRequest(AdditionalInfoRequest r) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            r.setId((int)session.save(r));
            tx.commit();
        }catch (HibernateException e){
            e.printStackTrace();
            if(tx!=null) tx.rollback();
            return null;
        } finally {
            session.close();
        }
        return r;
    }

    @Override
    public AdditionalInfoRequest updateAdditionalInfoRequest(AdditionalInfoRequest change) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            session.update(change);
            tx.commit();
        }catch (HibernateException e){
            e.printStackTrace();
            if(tx!=null) tx.rollback();
            return null;
        }finally {
            session.close();
        }
        return change;
    }

    @Override
    public AdditionalInfoRequest deleteAdditionalInfoRequest(int id) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        AdditionalInfoRequest r;
        try{
            tx = session.beginTransaction();
            r = session.get(AdditionalInfoRequest.class, id);
            session.delete(r);
            tx.commit();
        }catch (HibernateException e){
            e.printStackTrace();
            if(tx!=null) tx.rollback();
            return null;
        }finally {
            session.close();
        }
        return r;
    }
}
