package com.revature.repo;

import com.revature.models.GradingFormats;
import com.revature.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class GradingFormatsRepoImpl implements GradingFormatsRepo{
    @Override
    public GradingFormats getGradingFormat(int id) {
        Session session = HibernateUtil.getSession();
        GradingFormats g = null;
        try{
            g = session.get(GradingFormats.class, id);
        } catch (HibernateException e){
            e.printStackTrace();
        }finally {
            session.close();
        }
        return g;
    }

    @Override
    public List<GradingFormats> getAllGradingFormats() {
        Session session = HibernateUtil.getSession();
        List<GradingFormats> gradingFormatsList = null;
        try{
            gradingFormatsList = session.createQuery("FROM GradingFormats").list();
        }catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return gradingFormatsList;
    }

    @Override
    public GradingFormats addGradingFormat(GradingFormats g) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            g.setId((int)session.save(g));
            tx.commit();
        }catch (HibernateException e) {
            e.printStackTrace();
            if(tx!=null) tx.rollback();
            return null;
        } finally {
            session.close();
        }
        return g;
    }

    @Override
    public GradingFormats updateGradingFormat(GradingFormats change) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(change);
            tx.commit();
        } catch (HibernateException e){
            e.printStackTrace();
            if(tx!=null) tx.rollback();
            return null;
        } finally {
            session.close();
        }
        return change;
    }

    @Override
    public GradingFormats deleteGradingFormat(int id) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        GradingFormats g = null;
        try{
            tx = session.beginTransaction();
            g = session.get(GradingFormats.class, id);
            session.delete(g);
            tx.commit();
        } catch (HibernateException e) {
            e.printStackTrace();
            if(tx!=null) tx.rollback();
            return null;
        } finally {
            session.close();
        }
        return g;
    }
}
