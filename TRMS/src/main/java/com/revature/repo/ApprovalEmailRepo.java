package com.revature.repo;

import com.revature.models.ApprovalEmail;

import java.util.List;

public interface ApprovalEmailRepo {
    public ApprovalEmail getApprovalEmail(int id);
    public List<ApprovalEmail> getAllApprovalEmails();
    public ApprovalEmail addApprovalEmail(ApprovalEmail a);
    public ApprovalEmail updateApprovalEmail(ApprovalEmail a);
    public ApprovalEmail deleteApprovalEmail(int id);
}
