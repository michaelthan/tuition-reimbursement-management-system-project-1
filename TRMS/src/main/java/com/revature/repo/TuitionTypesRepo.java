package com.revature.repo;

import com.revature.models.TuitionTypes;

import java.util.List;

public interface TuitionTypesRepo {
    public TuitionTypes getTuitionTypes(int id);
    public List<TuitionTypes> getAllTuitionTypes();
    public TuitionTypes addTuitionType(TuitionTypes t);
    public TuitionTypes updateTuitionType(TuitionTypes change);
    public TuitionTypes deleteTuitionType(int id);
}
