package com.revature.repo;

import com.revature.models.Departments;

import java.util.List;

public interface DepartmentsRepo {
    public Departments getDepartment(int id);
    public List<Departments> getAllDepartments();
    public Departments addDepartment(Departments d);
    public Departments updateDepartment(Departments d);
    public Departments deleteDepartment(int id);
}
