package com.revature.repo;

import com.revature.models.Employee;

import java.util.List;

public interface EmployeeRepo {
    public Employee getEmployee (int id);
    public List<Employee> getAllEmployees();
    public Employee addEmployee(Employee e);
    public Employee updateEmployee(Employee e);
    public Employee deleteEmployee(int id);
}
