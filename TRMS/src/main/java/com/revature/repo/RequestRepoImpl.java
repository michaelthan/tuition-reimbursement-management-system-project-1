package com.revature.repo;

import com.revature.models.*;
import com.revature.util.HibernateUtil;
import com.revature.util.MyLogger;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.proxy.HibernateProxy;

import java.util.List;

public class RequestRepoImpl implements RequestRepo{
    @Override
    public Request getRequest(int id) {
        Session session = HibernateUtil.getSession();
        Request r;
        Transaction tx;
        try{
            tx = session.beginTransaction();
            r = session.get(Request.class,id);
            tx.commit();
            return r;
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return null;
    }

    @Override
    public List<Request> getAllRequests() {
        Session session = HibernateUtil.getSession();
        List<Request> RequestList = null;
        Transaction tx = null;
        try {
            RequestList = session.createQuery("FROM Request").list();
//            for (Request r : RequestList) {
//                r.setGradingFormat(r.getGradingFormat());
//                r.setTuitionType(r.getTuitionType());
//                r.setEmployee(r.getEmployee());
//                r.setStatus(r.getStatus());
//                r.setApprovalEmail(r.getApprovalEmail());
//                r.setPassingGradeDoc(r.getPassingGradeDoc());
//            }
        }catch (HibernateException e) {
            e.printStackTrace();
            tx.rollback();
        } finally {
            session.close();
        }
        return RequestList;
    }

    @Override
    public Request addRequest(Request r) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            r.setId((int)session.save(r));
            tx.commit();
            MyLogger.logger.info("creating request");
        }catch (HibernateException e){
            e.printStackTrace();
            if(tx!=null) tx.rollback();
            return null;
        } finally {
            session.close();
        }
        return r;
    }

    @Override
    public Request updateRequest(Request change) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            session.update(change);
            tx.commit();
        }catch (HibernateException e){
            e.printStackTrace();
            if(tx!=null) tx.rollback();
            return null;
        }finally {
            session.close();
        }
        return change;
    }

    @Override
    public Request deleteRequest(int id) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        Request r;
        try{
            tx = session.beginTransaction();
            r = session.get(Request.class, id);
            session.delete(r);
            tx.commit();
        }catch (HibernateException e){
            e.printStackTrace();
            if(tx!=null) tx.rollback();
            return null;
        }finally {
            session.close();
        }
        return r;
    }
}
