package com.revature.repo;

import com.revature.models.TuitionTypes;
import com.revature.models.TuitionTypes;
import com.revature.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class TuitionTypesRepoImpl implements TuitionTypesRepo{
    @Override
    public TuitionTypes getTuitionTypes(int id) {
        Session session = HibernateUtil.getSession();
        TuitionTypes r;
        try{
            r = session.get(TuitionTypes.class,id);
            return r;
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return null;
    }

    @Override
    public List<TuitionTypes> getAllTuitionTypes() {
        Session session = HibernateUtil.getSession();
        List<TuitionTypes> TuitionTypesList = null;
        try {
            TuitionTypesList = session.createQuery("FROM TuitionTypes").list();
        }catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return TuitionTypesList;
    }

    @Override
    public TuitionTypes addTuitionType(TuitionTypes r) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            r.setId((int)session.save(r));
            tx.commit();
        }catch (HibernateException e){
            e.printStackTrace();
            if(tx!=null) tx.rollback();
            return null;
        } finally {
            session.close();
        }
        return r;
    }

    @Override
    public TuitionTypes updateTuitionType(TuitionTypes change) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            session.update(change);
            tx.commit();
        }catch (HibernateException e){
            e.printStackTrace();
            if(tx!=null) tx.rollback();
            return null;
        }finally {
            session.close();
        }
        return change;
    }

    @Override
    public TuitionTypes deleteTuitionType(int id) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        TuitionTypes r;
        try{
            tx = session.beginTransaction();
            r = session.get(TuitionTypes.class, id);
            session.delete(r);
            tx.commit();
        }catch (HibernateException e){
            e.printStackTrace();
            if(tx!=null) tx.rollback();
            return null;
        }finally {
            session.close();
        }
        return r;
    }
}
