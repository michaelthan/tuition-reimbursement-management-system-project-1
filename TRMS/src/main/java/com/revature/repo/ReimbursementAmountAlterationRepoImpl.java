package com.revature.repo;

import com.revature.models.ReimbursementAmountAlteration;
import com.revature.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class ReimbursementAmountAlterationRepoImpl implements ReimbursementAmountAlterationRepo{
    @Override
    public ReimbursementAmountAlteration getReimbursementAmountAlteration(int id) {
        Session session = HibernateUtil.getSession();
        ReimbursementAmountAlteration r;
        try{
            r = session.get(ReimbursementAmountAlteration.class,id);
            return r;
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return null;
    }

    @Override
    public List<ReimbursementAmountAlteration> getAllReimbursementAmountAlterations() {
        Session session = HibernateUtil.getSession();
        List<ReimbursementAmountAlteration> ReimbursementAmountAlterationList = null;
        try {
            ReimbursementAmountAlterationList = session.createQuery("FROM ReimbursementAmountAlteration").list();
        }catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return ReimbursementAmountAlterationList;
    }

    @Override
    public ReimbursementAmountAlteration addReimbursementAmountAlteration(ReimbursementAmountAlteration r) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            r.setId((int)session.save(r));
            tx.commit();
        }catch (HibernateException e){
            e.printStackTrace();
            if(tx!=null) tx.rollback();
            return null;
        } finally {
            session.close();
        }
        return r;
    }

    @Override
    public ReimbursementAmountAlteration updateReimbursementAmountAlteration(ReimbursementAmountAlteration change) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            session.update(change);
            tx.commit();
        }catch (HibernateException e){
            e.printStackTrace();
            if(tx!=null) tx.rollback();
            return null;
        }finally {
            session.close();
        }
        return change;
    }

    @Override
    public ReimbursementAmountAlteration deleteReimbursementAmountAlteration(int id) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        ReimbursementAmountAlteration r;
        try{
            tx = session.beginTransaction();
            r = session.get(ReimbursementAmountAlteration.class, id);
            session.delete(r);
            tx.commit();
        }catch (HibernateException e){
            e.printStackTrace();
            if(tx!=null) tx.rollback();
            return null;
        }finally {
            session.close();
        }
        return r;
    }
}
