function logOut(){
    localStorage.setItem("user", null);
    window.location.href = "login.html";
}
function getUser(){
    return localStorage.getItem("user");
}
function goToCreateRequest(){
    window.location.href = "createrequest.html";
}