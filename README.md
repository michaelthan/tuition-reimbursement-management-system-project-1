# Tuition Reimbursement Management System project 1

## Project Description
Given a business document, a system was developed to manage Tuition Reimbursement workflow.

Business document stated a tuition reimbursement workflow system was needed for users of a company. An employee will add a request which will go to their supervisor to be approved. If the request is approved it will be sent to the department head. If the request is approved by the department head, the request is sent to the benefits coordinator. Once the benefits coordinator approves it, it will be pending a passing grade on the employee. The request can be denied at any step and each approval level can request more information.

## Technologies Used

* PostgreSQL
* Maven
* Java
* Hibernate
* JDBC
* JavaScript
* HTML/CSS
* JUNIT4
* Postman
* Cucumber
* Selenium

## Features

List of features ready and TODOs for future development
* Users can create a request
* Users can manage and view their own requests
* Users can manage/approve/deny requests that require their attention

To-do list/Possible Improvements:
* Display more details in the request display frontend
* Add security to our access

## Getting Started
   
To use this code create the database through PostgreSQL and run backend Java API. The user interface will be displayed through the HTML/CSS. A hibernate XML config file will be needed in the resources file to configure database connection.

## Usage

Use the HTML/CSS frontend to request from the backend API. The frontend will give the user an interface to go through the workflow of the Tuition Reimbursement Management System. A user can only be created through the database. All employees will be able to create a request which will go to the supervisor for approval. The lowest level employee will go from the supervisor to, department head, and benefits coordinator to be approved. Then once a passing grade document is submitted, the whole request will be approved and employee will receive reimbursement. If an employee is at a higher authorization level then they will follow the workflow from the authorization level they are at. For example, if a department head submits a request, it goes straight to the Benefits coordinator. If the request is from a benefits coordinator, the request will be sent to the CEO. If the request is created by the CEO, then it will be automatically approved.
