-- TRMS
create table departments(
	id serial primary key,
	department varchar(50)
);

insert into departments values(default, 'Human Resources');
insert into departments values(default, 'Billing');
insert into departments values(default, 'IT');
insert into departments values(default, 'Training');
insert into departments values(default, 'General');
insert into departments values(default, 'Sales');
insert into departments values(default, 'Marketing');
insert into departments values(default, 'Human Resources');
insert into departments values(default, 'Administration');

create table roles(
	id serial primary key,
	role varchar(50)
);

insert into roles values(default,'Employee');
insert into roles values(default,'Department Head');
insert into roles values(default,'Benefits Coordinator');
insert into roles values(default,'CEO');

create table grading_formats (
	id serial primary key,
	format varchar(50),
	default_pass varchar(50)
);

insert into grading_formats values(default, 'Letter Grade', 'C');
insert into grading_formats values(default, 'Percentage', '70');
insert into grading_formats values(default, 'Grade Point Average', '3.00');
insert into grading_formats values(default, 'Pass/Fail', 'Pass');

create table tuition_types (
	id serial primary key,
	tuition varchar(50),
	reimbursement_amount numeric
);

insert into tuition_types values(default, 'University Course', .8);
insert into tuition_types values(default, 'Seminars', .6);
insert into tuition_types values(default, 'Certification Prep Class', .75);
insert into tuition_types values(default, 'Certification', 1.0);
insert into tuition_types values(default, 'Technical Training', .9);
insert into tuition_types values(default, 'Others', .3);

create table request_statuses (
	id serial primary key,
	status varchar(50)
);

insert into request_statuses values(default, 'pending employee action');
insert into request_statuses values(default, 'pending supervisor approval');
insert into request_statuses values(default, 'pending employee action by supervisor');
insert into request_statuses values(default, 'pending pending department head approval');
insert into request_statuses values(default, 'pending employee action by department head');
insert into request_statuses values(default, 'pending BenCo approval');
insert into request_statuses values(default, 'pending employee action by BenCo');
insert into request_statuses values(default, 'pending CEO approval');
insert into request_statuses values(default, 'pending employee action by CEO');
insert into request_statuses values(default, 'denied by supervisor');
insert into request_statuses values(default, 'denied by department head');
insert into request_statuses values(default, 'denied by BenCo');
insert into request_statuses values(default, 'denied by supervisor');
insert into request_statuses values(default, 'denied by CEO');


drop table departments;
drop table roles;
drop table grading_formats;
drop table tuition_types;
drop table request_statuses;












